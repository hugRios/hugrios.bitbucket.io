function CargarParse()
{
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
}

var arrayAreas = ['RH', 'Administración y finanzas', 'Operaciones', 'Logística', 'Ventas', 'Marketing', 'Dirección general', 'Otra'];
var arrayGeneracion=['1945 or before', '1946-1964', '1965-1985', '1986-2000', 'after 2000'];
var arrayIndustria=['Automotriz', 'Consultora', 'Educación', 'Energía', 'Farmacéutica', 'Financiera', 'Legal', 'Retail', 'Servicios', 'Tecnología', 'Otro']
var arrayCheckAreas=[];

/*******************************************************************/
/*  Variables globales que se ocupan para almacenar las
    calificaciones t-top b-botoom r-rigth l-left                   */
/*******************************************************************/
var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0;
/*******************************************************************/
/*  Variables globales que se ocupan para almacenar el id del
    objeto area y la generacion                                    */
/*******************************************************************/
var a="",g="";
/*******************************************************************/
/*  Funciones que permiten la navegación entre paginas             */
/*******************************************************************/

/*******************************************************************/
/*  Variables globales para WellBeing que se ocupan para almacenar el id del
    objeto area, antiguedad y la generacion                                    */
/*******************************************************************/
var aWell="", gWell="", anti='', indWell, empWell, areaWell, antigWell, clienteWell;

function irTest()
{

    window.location="index2.html?Industria="+get("Industria")+"&Cliente="+get("Cliente")+"&Periodo="+get("Periodo");
}

function irInicio(){
  window.location="inicio.html?Industria="+get("Industria")+"&Cliente="+get("Cliente")+"&Periodo="+get("Periodo");
}

function irREsultados(){
  window.location="Resultados/admini.html";
}

function irResultadosFiltro(){
  console.log('sdsd');
}

function irWeelbeing()
{
    if(aWell!=''&&gWell!=''&&anti!=''&&indWell!=''){
      window.location="CircleYellow/index3.html?Area="+aWell+"&Generacion="+gWell+"&Antiguedad="+anti+"&Industria="+indWell+"&Periodo="+get("Periodo")+"";

    }else{
      alert('Debes elegir todos los aspectos necesarios para iniciar')
    }
}

function enviaResultados(){

}





//Envía a usuario a resultados por industria//
$(document).ready(function(){
  Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
  $("#industriaR").on('click','li',function (){
    OcultarTodo();
$("#industriaP").html($(this).text());
var industria = $(this).text();
if(industria == "Otro"){
  window.location="adminWellInd.html?Industria="+"igeBtdJDd1";
}else{
  var query = new Parse.Query('indWell');
      query.equalTo('Nombre', industria);
      query.find({
        success: function(res){
          indWell = res[0].id;
          window.location="adminWellInd.html?Industria="+indWell;
        }
      })
}


  });
})

//Envía a usuario a resultados por cliente//

$(document).ready(function(){
  Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
  $("#empresaR").on('click','li',function (){
    OcultarTodo();
$("#empresaP").html($(this).text());
var cliente = $(this).text();
var query = new Parse.Query('ClienteWell');
    query.equalTo('nombre', cliente);
    query.find({
      success: function(res){
        clienteWell = res[0].id;
        window.location="adminWellEmp.html?Cliente="+clienteWell;
      }
    })

  });
})



$(document).ready(function(){
  Parse.initialize("steelcaseCirclesAppId");
  Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';

  var query = new Parse.Query('ClienteWell');
      query.find({
        success: function(results){
          var list = document.createElement('li');
          for (var i = 0; i < results.length; i++) {

              $("#empresasWell").append('<li>'+results[i].get("nombre")+'</li>')
              $("#empresasWell li").attr("value", results[i].id)
          }
        }
      })
})

//devuelve áreas a dropdown
$(document).ready(function(){
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
  $("#areaR").on('click','li',function (){
    OcultarTodo();
$("#areaP").html($(this).text());
var area = $(this).text();

        window.location="adminWellArea.html?Area="+area;


  });
})




/*Function
Agrega áreas a dropdown de vista de resultados*/
$(document).ready(function(){
  var list = document.createElement('li');
  for (var i = 0; i < arrayAreas.length; i++) {
      $("#areaWell").append('<li>'+arrayAreas[i]+'</li>')
  }
})


/* Funcion que agrega las opciones de generacion */
/*$(document).ready(function(){
  var list = document.createElement('li');
  for (var i = 0; i < arrayGeneracion.length; i++) {
      $("#generaciones").append('<li>'+arrayGeneracion[i]+'</li>')
  }
})*/


/********************************************/
/* Funcion que agrega antiguedad en dropdown */
$(document).ready(function(){
  var Antiguedad = Parse.Object.extend("Antiguedad");
  var query = new Parse.Query(Antiguedad);
      query.find({
        success: function(res){
          for (var i = 0; i < res.length; i++) {
              $("#antigWell").append('<li id='+res[i].id+'>'+res[i].get("nombre")+'</li>');
          }
        }
      })
})
/*fUNCIÓN QUE OBTIENE ID Y MUESTRA LOS resultados
DE LA ANTIGUEDAD QUE EL USUARIO ELIGE */
$(document).ready(function(){

  $("#antigWell").on('click', 'li',function(){
    var id = $(this).attr('id');
    window.location="adminWellAntig.html?Antiguedad="+id;
  });
})



/********************************************/
/* Funcion que agrega generaciones en dropdown */
$(document).ready(function(){
  var Generacion = Parse.Object.extend("genWell");
  var query = new Parse.Query(Generacion);
      query.find({
        success: function(res){
          for (var i = 0; i < res.length; i++) {
              $("#genWell").append('<li id='+res[i].id+'>'+res[i].get("Nombre")+'</li>');
          }
        }
      })
})
/*fUNCIÓN QUE OBTIENE ID Y MUESTRA LOS resultados
DE LA GENERACIÓN QUE EL USUARIO ELIGE */
$(document).ready(function(){

  $("#genWell").on('click', 'li',function(){
    var id = $(this).attr('id');
    window.location="adminWellGen.html?Generacion="+id;
  });
})


/*********************************************************/
/* Funcion que obtiene valor de generaciones elegido */
$(document).ready(function(){
  $("#generacionD").on('click','li',function (){
    OcultarTodo();
$("#generacionP").html($(this).text());
var generacion = $(this).text();
//Parse
var query = new Parse.Query('genWell');
    query.equalTo('Nombre', generacion);
    query.find({
      success: function(res){
        gWell = res[0].id;
        console.log(gWell);
      }
    })
  });
})


$(document).ready(function(){
  Parse.initialize("steelcaseCirclesAppId");
  Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';

  var Industria = Parse.Object.extend("indWell");
  var query = new Parse.Query(Industria);
      query.find({
        success: function(res){
          for (var i = 0; i < res.length; i++) {
            $("#industriasWellNew").append($("<option></option>")
                          .attr("value",res[i].get("Nombre"))
                          .text(res[i].get("Nombre")));
          }
          $("#industriasWellNew").append($("<option></option>")
                        .attr("value","Otro")
                        .text("Otro"));
        }
      })
})


//Industrias
$(document).ready(function(){
  Parse.initialize("steelcaseCirclesAppId");
  Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';

  var Industria = Parse.Object.extend("indWell");
  var query = new Parse.Query(Industria);
      query.find({
        success: function(res){
          for (var i = 0; i < res.length; i++) {
                  $("#industriasWell").append  ('<input type = "checkbox" name ="indGrupo" id="'+res[i].id+'" value="'+res[i].id+'"><label for="'+res[i].get("Nombre")+'">'+res[i].get("Nombre")+'</label><br>');
          }
            $("#industriasWell").append('<button onclick="muestraRes();">'+"Mostrar"+"</button>")
        }
      })
})
/* Funcion que obtiene valor de industria elegida */
$(document).ready(function(){
  $("#industriaD").on('click','li',function (){
    OcultarTodo();
$("#industriaP").html($(this).text());
var industria = $(this).text();

var query = new Parse.Query('indWell');
    query.equalTo('Nombre', industria);
    query.find({
      success: function(res){
        indWell = res[0].id;
        console.log(indWell);
      }
    })

  });
})

function verificarInd(){

  if($("#industriasWellNew").val() == "Otro"){
    $("#newIndustria").show();
    return true;
  }else{
    $("#newIndustria").hide();
  }
}


function muestraIndustria()
{
  OcultarTodo();
  $("#industrias").show();
}



function addAntiguedad(anio){
    OcultarTodo();

  var valAntiguedad;
  var url = window.location.search;
  var cadena = url.substring(url.indexOf('&')+1, url.length);
  var cliente = cadena.substring(cadena.indexOf('=')+1, cadena.indexOf("&"));
  var periodo = cadena.substring(cadena.lastIndexOf('=')+1, cadena.length);

  Parse.initialize("steelcaseCirclesAppId");
  Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';

  var Antiguedad = Parse.Object.extend('Antiguedad');
  var queryAntiguedad = new Parse.Query('Antiguedad');

      queryAntiguedad.equalTo('nombre',anio);
      queryAntiguedad.find({
        success: function(res){
          anti = res[0].id;
        }
      })



    $("#areaAnt").html(anio);


}



function encuestaWell(){
//  $("#link").val("https://hugrios.bitbucket.io/inicio.html");
}

function addArea(){
  $("#agregaArea").show();
}

$(document).ready(function(){
  $( "#datepicker" ).datepicker();
})



//Funciones para validar Campos de nueva área y númro de empleados
function verifica(){

    var str =  $("#nuevaArea").val();
    if (!/^([A-Z\ a-z\ ñ\ Ñ\ ,\ ])*$/.test(str)){
      alert("el formato para agregar areas es: area, area2, area3")
      return false;
    }else{
      return true
    }

}

function verificaEmpleados(){

    console.log("dfdf");
    var str = $("#noEmpleados").val();
    if(!/^([0-9])*$/.test(str)){
      alert("Sólo puedes ingresar valores numéricos");
      $("#noEmpleados").val('');
    }

}



function getAreas(){
  if(verifica() == true){
    var arrayCheckbox = [];
    $.each($("input[name='user_group[]']:checked"), function() {
    arrayCheckbox.push($(this).val());
  });

  if($("#nuevaArea").val() !== ''){

      var areas = $("#nuevaArea").val();
      var arrayArea = areas.split(",");
      for (var i = 0; i < arrayArea.length; i++) {
        arrayCheckbox.push(arrayArea[i])
      }
  }
return arrayCheckbox;
}

}



function getNoRes(){
  var array = [];
  $.each($("input[name='indGrupo']:checked"), function(){
    array.push($(this).val());
  });
  return array;
}

function muestraRes(){
  var arregloInd = getNoRes();
  var industras;
if(arregloInd.length == 1){
  window.location="adminWellInd.html?Industria="+arregloInd[0];
}else if (arregloInd.length == 2) {
  console.log("son dos");
}
}




function generarWell(){ //genera link por cliente

  arrayCheckAreas = getAreas();

  var customerName =$("#newCliente").val();
  var noEmpleados = parseInt($("#noEmpleados").val());
  var industria = $("#industriasWellNew").val();
  var newInd =  $("#newIndustria").val();
  console.log(industria);
  var fecha =$("#datepicker").datepicker( 'getDate' );
  var link = "https://hugrios.bitbucket.io/inicio.html";
  var date = new Date(fecha);
  console.log(date);
  date.setHours(date.getHours()+23);
  date.setMinutes(date.getMinutes()+59);
  console.log(date);
try {
  if(arrayCheckAreas.length != 0 && customerName != '' && industria !='' && noEmpleados != '' ){


    var cliente = Parse.Object.extend('ClienteWell');
    var newCliente = new cliente();

    var Link = Parse.Object.extend("LinksWell");
    var newLink = new Link();


    var Industria = Parse.Object.extend('indWell');
    var newIndustria = new Industria();
    var query = new Parse.Query(Industria);
      query.equalTo("Nombre", industria);
      query.find({
        success: function(res){
          if(res.length == 0){// si no encuentra resultados para la industria se debe agregar un nueva
            newIndustria.set("Nombre", newInd);
            newIndustria.save(null,{
              success: function(newIndustria){
                newCliente.set("nombre",customerName);
                newCliente.set("noEmpleados", noEmpleados);
                newCliente.set("Industria", newIndustria);
                newCliente.save(null,{
                  success: function(newCliente){
                    for (var i = 0; i < arrayCheckAreas.length; i++) {
                      var areas = Parse.Object.extend('areaWell');
                      var newAreas = new areas();
                      newAreas.set('cliente', newCliente);
                      newAreas.set('Name', arrayCheckAreas[i]);
                      newAreas.save();
                      console.log(i)
        //termina creación areas con cliente
                    }
                    newLink.set("cliente", newCliente);
                    newLink.set("link", link+"?Cliente="+newCliente.id+"&Industria="+newIndustria.id);
                    newLink.set("fechaLimite", date);
                    newLink.save();
                    $("#link").val("https://hugrios.bitbucket.io/inicio.html"+"?Cliente="+newCliente.id+"&Industria="+newIndustria.id);
                  }//
                })//termina creación del cliente
              }
            })


          }else{
            newCliente.set("nombre",customerName);
            newCliente.set("noEmpleados", noEmpleados);
            newCliente.set("Industria", res[0]);
            newCliente.save(null,{
              success: function(newCliente){
                for (var i = 0; i < arrayCheckAreas.length; i++) {
                  var areas = Parse.Object.extend('areaWell');
                  var newAreas = new areas();
                  newAreas.set('cliente', newCliente);
                  newAreas.set('Name', arrayCheckAreas[i]);
                  newAreas.save();
                  console.log(i)
    //termina creación areas con cliente
                }
                newLink.set("cliente", newCliente);
                newLink.set("link", link+"?Cliente="+newCliente.id+"&Industria="+res[0].id);
                newLink.set("fechaLimite", date);
                newLink.save();
                $("#link").val("https://hugrios.bitbucket.io/inicio.html"+"?Cliente="+newCliente.id+"&Industria="+res[0].id);
              }//
            })//termina creación del cliente
          }
        }
      })
  }else{
    alert("Recuerda: debes selecionar industría, área y agregar un cliente para continuar")
  }

} catch (e) {
  console.log("error"+e)
}

}





/*******************************************************************/
/*  Funciones que permiten obtener un valor de la url
    Input:  name->String con el nombre de la variable
    Output: valor de la variable                                   */
/*******************************************************************/
function get(name)
{
    var url = window.location.search;
    var num = url.search(name);
    var namel = name.length;
    var frontlength = namel+num+1;
    var front = url.substring(0, frontlength);
    url = url.replace(front, "");
    num = url.search("&");

    if(num>=0) return url.substr(0,num);
    if(num<0)  return url;
}
/*******************************************************************/
/*  Name: enviarEncuesta;
    Function: valida que todos los campos tengan un valor
            y anexa a la base de datos un elemento de la clase
            encuesta;
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function enviarEncuesta()
{
    if(t>0&&rt>0&&r>0&&rb>0&&b>0&&lb>0&&l>0&&lt>0&&a!=""&&g!="")
    {
        Parse.initialize("steelcaseCirclesAppId");
        Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';

        var Industria = Parse.Object.extend("Industria");
        var query = new Parse.Query(Industria);
        query.get(get("Industria"),{
        success: function(ind) {
            var Cliente = Parse.Object.extend("Cliente");
            var query = new Parse.Query(Cliente);
            query.get(get("Cliente"),{
            success: function(cl) {
                var Periodo = Parse.Object.extend("Periodo");
                var query = new Parse.Query(Periodo);
                query.get(get("Periodo"),{
                success: function(period) {
                    var Area = Parse.Object.extend("Area");
                    var query = new Parse.Query(Area);
                    query.get(a,{
                    success: function(ar) {
                        var Generacion = Parse.Object.extend("Generacion");
                        var query = new Parse.Query(Generacion);
                        query.get(g,{
                        success: function(ge) {
                            if(true)
                            {
                                var Evaluacion = Parse.Object.extend("Evaluacion");
                                var newEvaluacion = new Evaluacion();
                                newEvaluacion.set("generacion", ge);
                                newEvaluacion.set("area", ar);
                                newEvaluacion.set("cliente", cl);
                                newEvaluacion.set("industria", ind);
                                newEvaluacion.set("physical", lt);
                                newEvaluacion.set("choice", t);
                                newEvaluacion.set("posture", rt);
                                newEvaluacion.set("control", b);
                                newEvaluacion.set("presence", rb);
                                newEvaluacion.set("emotional", lb);
                                newEvaluacion.set("privacy", r);
                                newEvaluacion.set("cognitive", l);
                                newEvaluacion.set("periodo", period);
                                if(confirm('¿Desea terminar la encuesta?'))
                                {
                                    newEvaluacion.save(null,{success:function(){
                                    window.location="gracias.html";
                                    return true;
                                    }, error:function(error){
                                        alert("Failed to create new object, with error code: " + error.message);
                                    }});
                                }

                            }
                        },
                        error: function(error) {
                            alert("Error: " + error.code + " " + error.message);
                        }
                        });
                    },
                    error: function(error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                    });
                },
                error: function(error) {
                alert("Error: " + error.code + " " + error.message);
                }
                });
            },
            error: function(error) {
                alert("Error: " + error.code + " " + error.message);
            }
            });
        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });



    }
    else
    {
        alert("Falta calificar algún area");
        return false;
    }
}




/*******************************************************************/
/*  Permite que al hacer clic sobre los botones de calificación
    estos guarden el valor dado en las variables globales
    't', 'r','b','l','rt' 'rb', 'lt', 'lb'. Y cambia visualmente
    el boton activo.
    Entradas:
        id: Id del objeto html                                     */
/*******************************************************************/
function asignarCalificacion(id,e)
{
    asignarPosicion(id,e);

    switch(id)
    {
        case "r1":
        r=1;
        for(var i =0; i<=5; i++)
        {
            $("#r"+i).children().removeClass("active");
        }
        $("#r1").children().addClass("active");

        break;
        case "r2":
        r=2;
        for(var i =0; i<=5; i++)
        {
            $("#r"+i).children().removeClass("active");
        }
        $("#r2").children().addClass("active");
        break;
        case "r3":
        r=3;
        for(var i =0; i<=5; i++)
        {
            $("#r"+i).children().removeClass("active");
        }
        $("#r3").children().addClass("active");
        break;
        case "r4":
        r=4;
        for(var i =0; i<=5; i++)
        {
            $("#r"+i).children().removeClass("active");
        }
        $("#r4").children().addClass("active");
        break;
        case "r5":
        r=5;
        for(var i =0; i<=5; i++)
        {
            $("#r"+i).children().removeClass("active");
        }
        $("#r5").children().addClass("active");
        break;
        case "l1":
        l=1;
        for(var i =0; i<=5; i++)
        {
            $("#l"+i).children().removeClass("active");
        }
        $("#l1").children().addClass("active");
        break;
        case "l2":
        l=2;
        for(var i =0; i<=5; i++)
        {
            $("#l"+i).children().removeClass("active");
        }
        $("#l2").children().addClass("active");
        break;
        case "l3":
        l=3;
        for(var i =0; i<=5; i++)
        {
            $("#l"+i).children().removeClass("active");
        }
        $("#l3").children().addClass("active");
        break;
        case "l4":
        l=4;
        for(var i =0; i<=5; i++)
        {
            $("#l"+i).children().removeClass("active");
        }
        $("#l4").children().addClass("active");
        break;
        case "l5":
        l=5;
        for(var i =0; i<=5; i++)
        {
            $("#l"+i).children().removeClass("active");
        }
        $("#l5").children().addClass("active");
        break;
        case "t1":
        t=1;
        for(var i =0; i<=5; i++)
        {
            $("#t"+i).children().removeClass("active");
        }
        $("#t1").children().addClass("active");
        break;
        case "t2":
        t=2;
        for(var i =0; i<=5; i++)
        {
            $("#t"+i).children().removeClass("active");
        }
        $("#t2").children().addClass("active");
        break;
        case "t3":
        t=3;
        for(var i =0; i<=5; i++)
        {
            $("#t"+i).children().removeClass("active");
        }
        $("#t3").children().addClass("active");
        break;
        case "t4":
        t=4;
        for(var i =0; i<=5; i++)
        {
            $("#t"+i).children().removeClass("active");
        }
        $("#t4").children().addClass("active");
        break;
        case "t5":
        t=5;
        for(var i =0; i<=5; i++)
        {
            $("#t"+i).children().removeClass("active");
        }
        $("#t5").children().addClass("active");
        break;
        case "b1":
        b=1;
        for(var i =0; i<=5; i++)
        {
            $("#b"+i).children().removeClass("active");
        }
        $("#b1").children().addClass("active");
        break;
        case "b2":
        b=2;
        for(var i =0; i<=5; i++)
        {
            $("#b"+i).children().removeClass("active");
        }
        $("#b2").children().addClass("active");
        break;
        case "b3":
        b=3;
        for(var i =0; i<=5; i++)
        {
            $("#b"+i).children().removeClass("active");
        }
        $("#b3").children().addClass("active");
        break;
        case "b4":
        b=4;
        for(var i =0; i<=5; i++)
        {
            $("#b"+i).children().removeClass("active");
        }
        $("#b4").children().addClass("active");
        break;
        case "b5":
        b=5;
        for(var i =0; i<=5; i++)
        {
            $("#b"+i).children().removeClass("active");
        }
        $("#b5").children().addClass("active");
        break;
        case "rt1":
        rt=1;
        for(var i =0; i<=5; i++)
        {
            $("#rt"+i).children().removeClass("active");
        }
        $("#rt1").children().addClass("active");
        break;
        case "rt2":
        rt=2;
        for(var i =0; i<=5; i++)
        {
            $("#rt"+i).children().removeClass("active");
        }
        $("#rt2").children().addClass("active");
        break;
        case "rt3":
        rt=3;
        for(var i =0; i<=5; i++)
        {
            $("#rt"+i).children().removeClass("active");
        }
        $("#rt3").children().addClass("active");
        break;
        case "rt4":
        rt=4;
        for(var i =0; i<=5; i++)
        {
            $("#rt"+i).children().removeClass("active");
        }
        $("#rt4").children().addClass("active");
        break;
        case "rt5":
        rt=5;
        for(var i =0; i<=5; i++)
        {
            $("#rt"+i).children().removeClass("active");
        }
        $("#rt5").children().addClass("active");
        break;
        case "rb1":
        rb=1;
        for(var i =0; i<=5; i++)
        {
            $("#rb"+i).children().removeClass("active");
        }
        $("#rb1").children().addClass("active");
        break;
        case "rb2":
        rb=2;
        for(var i =0; i<=5; i++)
        {
            $("#rb"+i).children().removeClass("active");
        }
        $("#rb2").children().addClass("active");
        break;
        case "rb3":
        rb=3;
        for(var i =0; i<=5; i++)
        {
            $("#rb"+i).children().removeClass("active");
        }
        $("#rb3").children().addClass("active");
        break;
        case "rb4":
        rb=4;
        for(var i =0; i<=5; i++)
        {
            $("#rb"+i).children().removeClass("active");
        }
        $("#rb4").children().addClass("active");
        break;
        case "rb5":
        rb=5;
        for(var i =0; i<=5; i++)
        {
            $("#rb"+i).children().removeClass("active");
        }
        $("#rb5").children().addClass("active");
        break;
        case "lt1":
        lt=1;
        for(var i =0; i<=5; i++)
        {
            $("#lt"+i).children().removeClass("active");
        }
        $("#lt1").children().addClass("active");
        break;
        case "lt2":
        lt=2;
        for(var i =0; i<=5; i++)
        {
            $("#lt"+i).children().removeClass("active");
        }
        $("#lt2").children().addClass("active");
        break;
        case "lt3":
        lt=3;
        for(var i =0; i<=5; i++)
        {
            $("#lt"+i).children().removeClass("active");
        }
        $("#lt3").children().addClass("active");
        break;
        case "lt4":
        lt=4;
        for(var i =0; i<=5; i++)
        {
            $("#lt"+i).children().removeClass("active");
        }
        $("#lt4").children().addClass("active");
        break;
        case "lt5":
        lt=5;
        for(var i =0; i<=5; i++)
        {
            $("#lt"+i).children().removeClass("active");
        }
        $("#lt5").children().addClass("active");
        break;
        case "lb1":
        lb=1;
        for(var i =0; i<=5; i++)
        {
            $("#lb"+i).children().removeClass("active");
        }
        $("#lb1").children().addClass("active");
        break;
        case "lb2":
        lb=2;
        for(var i =0; i<=5; i++)
        {
            $("#lb"+i).children().removeClass("active");
        }
        $("#lb2").children().addClass("active");
        break;
        case "lb3":
        lb=3;
        for(var i =0; i<=5; i++)
        {
            $("#lb"+i).children().removeClass("active");
        }
        $("#lb3").children().addClass("active");
        break;
        case "lb4":
        lb=4;
        for(var i =0; i<=5; i++)
        {
            $("#lb"+i).children().removeClass("active");
        }
        $("#lb4").children().addClass("active");
        break;
        case "lb5":
        lb=5;
        for(var i =0; i<=5; i++)
        {
            $("#lb"+i).children().removeClass("active");
        }
        $("#lb5").children().addClass("active");
        break;
    }
}


/*******************************************************************/
/*  Variables necesarias para crear el canvas que contien
    las lineas que unen los puntos con las calificaciones          */
/*******************************************************************/
var postx;
var posty;
var posrtx;
var posrty;
var posrx;
var posry;
var posrbx;
var posrby;
var posbx;
var posby;
var poslbx;
var poslby;
var poslx;
var posly;
var posltx;
var poslty;

/*******************************************************************/
/*  Name: iniciarPosiciones;
    Function: obtine la posicion del canvas y toma su punto medio
            para que todas las lineas tengan su origen en
            el centro
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/

function iniciarPosiciones(e)
{
    var can=$("#myCanvas").offset();
    if(e!=null)
    {
        var posx=e.pageX-can.left;
        var posy=e.pageY-can.top;
    }


    postx = posx+$("#myCanvas").width()/2;
    posty = posy+$("#myCanvas").height()/2;
    posrtx = posx+$("#myCanvas").width()/2;
    posrty = posy+$("#myCanvas").height()/2;
    posrx = posx+$("#myCanvas").width()/2;
    posry = posy+$("#myCanvas").height()/2;
    posrbx = posx+$("#myCanvas").width()/2;
    posrby = posy+$("#myCanvas").height()/2;
    posbx = posx+$("#myCanvas").width()/2;
    posby = posy+$("#myCanvas").height()/2;
    poslbx = posx+$("#myCanvas").width()/2;
    poslby = posy+$("#myCanvas").height()/2;
    poslx = posx+$("#myCanvas").width()/2;
    posly = posy+$("#myCanvas").height()/2;
    posltx = posx+$("#myCanvas").width()/2;
    poslty = posy+$("#myCanvas").height()/2;
}

/*******************************************************************/
/*  Name: asignarPosicion;
    Function: al dar click sobre un boton y asignar una calificacion
            se toma la posicion del mouse para agregar un valor
            a las lineas que conformaran el canvas.
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/

function asignarPosicion(id,e)
{
    var can=$("#myCanvas").offset();

    var posx=e.pageX-can.left;
    var posy=e.pageY-can.top;

    var identificador=id.substring(0,id.length-1);
    switch(identificador)
    {
        case "r":
            posrx=posx;
            posry=posy;
        break;
        case "l":
            poslx=posx;
            posly=posy;
        break;
        case "t":
            postx=posx;
            posty=posy;
        break;
        case "b":
            posbx=posx;
            posby=posy;
        break;
        case "rt":
            posrtx=posx;
            posrty=posy;
        break;
        case "rb":
            posrbx=posx;
            posrby=posy;
        break;
        case "lt":
            posltx=posx;
            poslty=posy;
        break;
        case "lb":
            poslbx=posx;
            poslby=posy;
        break;
    }
    crearCanvas();
}

function generarColor()
{
    var R=numero=Math.floor((Math.random() * 255));
    var G=numero=Math.floor((Math.random() * 255));
    var B=numero=Math.floor((Math.random() * 255));
    var color = "#"+ R.toString(16)+ G.toString(16)+ B.toString(16);
    return color;
}

/*******************************************************************/
/*  Name: crearCanvas;
    Function: Tomando los puntos default o los ya asignados por
            el usuario genera un canvas que permite visualizar las
            lineas que unen los aspectos calificados;
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function obtenerValoresParaElCanvas(t,rt,r,rb,b,lb,l,lt,color)
{
    var can=$("#myCanvas").offset();


    var post=$("#t"+t).children().offset();
    var posrt=$("#rt"+rt).children().offset();
    var posr=$("#r"+r).children().offset();
    var posrb=$("#rb"+rb).children().offset();
    var posb=$("#b"+b).children().offset();
    var poslb=$("#lb"+lb).children().offset();
    var posl=$("#l"+l).children().offset();
    var poslt=$("#lt"+lt).children().offset();

    var x1=post.left-can.left+($("#t"+t).children().width()/2);
    var x2=posrt.left-can.left+($("#rt"+t).children().width()/2);
    var x3=posr.left-can.left+($("#r"+t).children().width()/2);
    var x4=posrb.left-can.left+($("#rb"+t).children().width()/2);
    var x5=posb.left-can.left+($("#b"+t).children().width()/2);
    var x6=poslb.left-can.left+($("#lb"+t).children().width()/2);
    var x7=posl.left-can.left+($("#l"+t).children().width()/2);
    var x8=poslt.left-can.left+($("#lt"+t).children().width()/2);

    var y1=post.top-can.top+($("#t"+t).children().height()/2);
    var y2=posrt.top-can.top+($("#rt"+t).children().height()/2);
    var y3=posr.top-can.top+($("#r"+t).children().height()/2);
    var y4=posrb.top-can.top+($("#rb"+t).children().height()/2);
    var y5=posb.top-can.top+($("#b"+t).children().height()/2);
    var y6=poslb.top-can.top+($("#lb"+t).children().height()/2);
    var y7=posl.top-can.top+($("#l"+t).children().height()/2);
    var y8=poslt.top-can.top+($("#lt"+t).children().height()/2);

    abrirCanvas(x1,y1,x2,y2,x3,y3,x4,y4,x5,y5,x6,y6,x7,y7,x8,y8,color);
}




/*******************************************************************/
/*  Name: abrirCanvas;
    Function: Tomando los puntos default o los ya asignados por
            el usuario genera un canvas que permite visualizar las
            lineas que unen los aspectos calificados;
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function abrirCanvas(tx,ty,rtx,rty,rx,ry,rbx,rby,bx,by,lbx,lby,lx,ly,ltx,lty,color)
{
    var colorLinea="";
    if(color==0)
    {
        colorLinea="#7F1B7E";
    }
    else
    {
        colorLinea=color;
    }

    $('#myCanvas')
    .drawLine({
    strokeStyle: colorLinea,//color de la linea
    strokeWidth: 1,
    x1: tx, y1: ty,
    x2: rtx, y2: rty,
    x3: rx, y3: ry,
    x4: rbx, y4: rby,
    x5: bx, y5: by,
    x6: lbx, y6: lby,
    x7: lx, y7: ly,
    x8: ltx, y8: lty,
    x9: tx, y9: ty

    });
}

/*******************************************************************/
/*  Name: limpiarCanvas;
    Function: Limpia el canvas
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function limpiarCanvas()
{
    $('#myCanvas')
    .clearCanvas({ //borramos el canvas y luego lo volvemos a dibujar

    });
}



/*******************************************************************/
/*  Name: crearCanvas;
    Function: Tomando los puntos default o los ya asignados por
            el usuario genera un canvas que permite visualizar las
            lineas que unen los aspectos calificados;
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function crearCanvas()
{
    $('#myCanvas')
    .clearCanvas({ //borramos el canvas y luego lo volvemos a dibujar

    })
    .drawLine({
    strokeStyle: '#7F1B7E',//color de la linea
    strokeWidth: 1,
    x1: postx, y1: posty,
    x2: posrtx, y2: posrty,
    x3: posrx, y3: posry,
    x4: posrbx, y4: posrby,
    x5: posbx, y5: posby,
    x6: poslbx, y6: poslby,
    x7: poslx, y7: posly,
    x8: posltx, y8: poslty,
    x9: postx, y9: posty

    });
}



/*******************************************************************/
/*  Las siguientes funcionas se usan para tomar los valores
    del area y generacion proporcionadas por el usuario
    y a se almacenan en las variables a y g                        */
/*******************************************************************/
function seleccionarArea(id)
{
    var datos=id.split("*");
    a=datos[1];
    $("#areaP").html(datos[0]);
    //mostrarAreaDD();
    OcultarTodo();
}
function seleccionarGeneracion(id,generacion)
{
    var datos=id.split("*");
    g=datos[1];
    $("#generacionP").html(datos[0]);
    OcultarTodo();
    //mostrarGeneracionDD();
}

/*******************************************************************/
/*  Las siguiente funcion se usa para desplegar el texto que
    describe cada factor, dependiendo del factor seleccionado      */
/*******************************************************************/
function seleccionarFactor(factor)
{
    switch (factor) {
        case "t":
            $("#info").find("h3").html("ELECCIÓN");
            $("#info").find("P").html("Poder escoger: Donde trabajar dependiendo de tus actividades laborales.");
            $("#info").css("display","block");
            break;
        case "rt":
            $("#info").find("h3").html("POSTURA");
            $("#info").find("P").html("Movimiento y opciones: El espacio de trabajo debería fomentar el movimiento frecuente a lo largo del día y ofrecer opciones a las personas para qeu estas puedan trabajar sentadas, de pie o en posturas lounge.");
            $("#info").css("display","block");
            break;
        case "r":
            $("#info").find("h3").html("PRIVACIDAD");
            $("#info").find("P").html("Concentración y descanso: El entorno laboral debería proporcionar espacios que ofrezcan varias maneras de obtener privacidad, tanto en espacios abiertos como cerrados. La privacidad es muy importante para las personas y es un componente vital de la concentración y el descanso, fundamental para lograr el compromiso de los trabajadores.");
            $("#info").css("display","block");
            break;
        case "rb":
            $("#info").find("h3").html("PRESENCIA");
            $("#info").find("P").html("Digital y analógico, físico y virtual: Los espacios deberían permitir mantener interacciones de calidad con equipos tanto locales como distribuidos globalmente y en husos horairos, permitiendo.");
            $("#info").css("display","block");
            break;
        case "b":
            $("#info").find("h3").html("CONTROL");
            $("#info").find("P").html("Poder escoger: Como trabajar dependiendo de tus actividades laborales.");
            $("#info").css("display","block");
            break;
        case "lb":
            $("#info").find("h3").html("EMOCIONAL");
            $("#info").find("P").html("Sentirse Mejor: Fomentar la naturaleza social del trabajo creando espacios que promuevan las conexiones entre las personas y la organización y permitan a los trabajadores sentir que forman parte de algo importante.");
            $("#info").css("display","block");
            break;
        case "l":
            $("#info").find("h3").html("COGNITIVO");
            $("#info").find("P").html("Pensar Mejor: Satisfacer la necesidad de los trabajadores de concentrarse y descansar mediante espacios donde tanto los individuos como los equipos puedan pensar claramente, concentrarse con facilidad, resolver problemas y generar nuevas ideas.");
            $("#info").css("display","block");
            break;
        case "lt":
            $("#info").find("h3").html("FÍSICO");
            $("#info").find("P").html("Una mejor salud: Fomentar el movimiento a lo largo del día y la adopción de posturas saludables que ayuden a las personas a estar cómodas y llenas de energía.");
            $("#info").css("display","block");
            break;
        case "ll":
            $("#info").find("h3").html("BIENESTAR");
            $("#info").find("P").html(" Para mejorar el grado de compromiso de los trabajadores, el espacio de trabajo debe diseñarse de forma que fomente el bienestar desde un punto de vista holístico, incluyendo las necesidades físicas, cognitivas y emocionales de las personas.");
            $("#info").css("display","block");
            break;
        case "rr":
            $("#info").find("h3").html("ECOSISTEMA DE ESPACIOS");
            $("#info").find("P").html("En el trabajo, las personas necesitan a lo largo del día concentrarse, colaborar, descansar, relacionarse y aprender. El espacio de trabjao debería diseñarse como un ecosistema de zonas y entornos interconectados qeu sean destinos donde las personas tengan opciones y el control sobre dónde y cómo trabajar.");
            $("#info").css("display","block");
            break;
        default:
            break;
    }
}






/*******************************************************************/
/*  La siguiente funcion es llamada cuando el usuario escogue
    la o las industrias de las que desea un reporte                */
/*******************************************************************/
function seleccionarIndustria()
{
    var industriasActivas ="";
     $("input[name='industria']:checked").each ( function() {
   			industriasActivas += $(this).val() + "*";
        //chkId = chkId.slice(0, -1);
 	  });
    $(".checkbox").css("color","#000000");
    mostrarDatosIndustria(industriasActivas);

}


var ct = 0;
var crt = 0;
var cr = 0;
var crb = 0;
var cb = 0;
var clb = 0;
var cl = 0;
var clt = 0;
var encuestas=0;
var tabla="";


/*******************************************************************/
/*  Se inicializan las variables de calificaion de las areas,
    la variable encuesta y la variable table, se formatean las
    industrias que se van a buscas y se deja el circulo sin botones
    seleccionados, para que puedan marcarse los valores
    obtenidos por la base de datos                                 */
/*******************************************************************/
function mostrarDatosIndustria(industriasActivas)
{
    ct = 0;
    crt = 0;
    cr = 0;
    crb = 0;
    cb = 0;
    clb = 0;
    cl = 0;
    clt = 0;
    encuestas=0;
    industriasActivas=industriasActivas.substring(0,industriasActivas.length-1);
    var industrias = industriasActivas.split("*");
    tabla = "<table class='table'><thead><tr><td style='border-right: solid #404041 1px;'>   </td><td>Elección</td><td>Postura</td><td>Privacidad</td><td>Presencia</td><td>Control</td><td>Emocional</td><td>Cognitivo</td><td>Físico</td></tr></thead><tbody>";
    limpiarValoresDelCirculo();
    limpiarCanvas();

    recursividadTabla(industrias,0);
}
/*******************************************************************/
/*  Debido a que es necesario una anidación para poder
    llevar a cabo la sincronizacion esta funcion permite el
    orden de los querys y que todo se ejecute sin problemas
    de sincronizacion gracias a la recursividadTabla               */
/*******************************************************************/
function recursividadTabla(industrias,j)
{
        t = 0;
        rt = 0;
        r = 0;
        rb = 0;
        b = 0;
        lb = 0;
        l = 0;
        lt = 0;
        var Industria = Parse.Object.extend("Industria");
        var query = new Parse.Query(Industria);
        query.equalTo("objectId",industrias[j]);
        query.find({
        success: function(results) {
            var industriaID = results[0];
            var Evaluacion = Parse.Object.extend("Evaluacion");
            var query = new Parse.Query(Evaluacion);
            query.equalTo("industria",industriaID);
            query.find({
            success: function(results) {
                if(results.length>0)
                {
                    for (var i = 0; i < results.length; i++) {
                    var object = results[i];

                    //t+=object.get("physical");
                    t+=object.get("choice");
                    //rt+=object.get("choice");
                    rt+=object.get("posture");
                    //r+=object.get("posture");
                    r+=object.get("privacy");
                    //rb+=object.get("control");
                    rb+=object.get("presence");
                    //b+=object.get("presence");
                    b+=object.get("control");
                    lb+=object.get("emotional");
                    //l+=object.get("privacy");
                    l+=object.get("cognitive");
                    //lt+=object.get("cognitive");
                    lt+=object.get("physical");
                    }

                    var fct = (t/results.length).toFixed(1);
                    var fcrt = (rt/results.length).toFixed(1);
                    var fcr = (r/results.length).toFixed(1);
                    var fcrb = (rb/results.length).toFixed(1);
                    var fcb = (b/results.length).toFixed(1);
                    var fclb = (lb/results.length).toFixed(1);
                    var fcl = (l/results.length).toFixed(1);
                    var fclt = (lt/results.length).toFixed(1);

                    ct += t;
                    crt += rt;
                    cr += r;
                    crb += rb;
                    cb += b;
                    clb += lb;
                    cl += l;
                    clt += lt;
                    var color = $("#"+industrias[j]).children().attr('id');
                    $("#"+industrias[j]).css("color",color);
                    obtenerValoresParaElCanvas(Math.round(t/results.length),Math.round(rt/results.length),Math.round(r/results.length),Math.round(rb/results.length),Math.round(b/results.length),Math.round(lb/results.length),Math.round(l/results.length),Math.round(lt/results.length),color);
                    encuestas+=results.length;
                    tabla+="<tr><td style='border-right: solid #404041 1px;'>"+industriaID.get('nombre')+"</td><td class='tableTD'>"+fct+"</td><td class='tableTD'>"+fcrt+"</td><td class='tableTD'>"+fcr+"</td><td class='tableTD'>"+fcrb+"</td><td class='tableTD'>"+fcb+"</td><td class='tableTD'>"+fclb+"</td><td class='tableTD'>"+fcl+"</td><td class='tableTD'>"+fclt+"</td></tr>";
                }

                j++;
                if(j<industrias.length)
                {
                    recursividadTabla(industrias,j);
                }
                else
                {
                    tabla+="</tbody></table>";
                    var pTotal=((ct+crt+cr+crb+cb+clb+cl+clt)/encuestas)/8;
                    ct = Math.round(ct/encuestas);
                    crt = Math.round(crt/encuestas);
                    cr = Math.round(cr/encuestas);
                    crb = Math.round(crb/encuestas);
                    cb = Math.round(cb/encuestas);
                    clb = Math.round(clb/encuestas);
                    cl = Math.round(cl/encuestas);
                    clt = Math.round(clt/encuestas);
                    $("#rt"+crt).children().addClass("active");
                    $("#t"+ct).children().addClass("active");
                    $("#r"+cr).children().addClass("active");
                    $("#rb"+crb).children().addClass("active");
                    $("#b"+cb).children().addClass("active");
                    $("#lb"+clb).children().addClass("active");
                    $("#l"+cl).children().addClass("active");
                    $("#lt"+clt).children().addClass("active");
                    $("#tabla").html(tabla);
                    $("#promedioTotal").html("PROMEDIO TOTAL: " +pTotal.toFixed(1));
		            $("#encuestas").html("ENCUESTAS: "+encuestas);
                    obtenerValoresParaElCanvas(ct,crt,cr,crb,cb,clb,cl,clt,0);
                }
            },
            error: function(error) {
                alert("Error: " + error.code + " " + error.message);
            }
            });
        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });
}



/*******************************************************************/
/*  La siguiente funcion es llamada cuando el usuario escogue
    la o las industrias de las que desea un reporte                */
/*******************************************************************/
function seleccionarCliente()
{
    var cliente ="";
     $("input[name='cliente']:checked").each ( function() {
   			cliente += $(this).val() + "*";
        //chkId = chkId.slice(0, -1);
 	  });
      $(".checkbox").css("color","#000000");
    /*Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
    var Cliente = Parse.Object.extend("Cliente");
    var query = new Parse.Query(Cliente);
    query.get(cliente.substr(0,cliente.length-1),{
    success: function(results) {
        $("#headerClientes").html(results.get("nombre"));
    }
    });*/

    //mostrarDatosCliente(cliente);
    mostrarDatosCliente(cliente);

}

/*******************************************************************/
/*  Se inicializan las variables de calificaion de las areas,
    la variable encuesta y la variable table, se formatean las
    industrias que se van a buscas y se deja el circulo sin botones
    seleccionados, para que puedan marcarse los valores
    obtenidos por la base de datos                                 */
/*******************************************************************/
function mostrarDatosCliente(cliente)
{
    ct = 0;
    crt = 0;
    cr = 0;
    crb = 0;
    cb = 0;
    clb = 0;
    cl = 0;
    clt = 0;
    encuestas=0;
    cliente=cliente.substring(0,cliente.length-1);
    var clientes=cliente.split("*");
    tabla = "<table class='table'><thead><tr><td style='border-right: solid #404041 1px;'>   </td><td>Elección</td><td>Postura</td><td>Privacidad</td><td>Presencia</td><td>Control</td><td>Emocional</td><td>Cognitivo</td><td>Físico</td></tr></thead><tbody>";
    limpiarValoresDelCirculo();
    limpiarCanvas();
    recursividadTablaCliente(clientes,0);
}
/*******************************************************************/
/*  Name: limpiarValoresDelCirculo;
    Function: visualmente setea todos los elementos en desactivado
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function limpiarValoresDelCirculo()
{
    for(var i =0; i<=5; i++)
    {
        $("#t"+i).children().removeClass("active");
    }
    for(var i =0; i<=5; i++)
    {
        $("#rt"+i).children().removeClass("active");
    }
    for(var i =0; i<=5; i++)
    {
        $("#r"+i).children().removeClass("active");
    }
    for(var i =0; i<=5; i++)
    {
        $("#rb"+i).children().removeClass("active");
    }
    for(var i =0; i<=5; i++)
    {
        $("#b"+i).children().removeClass("active");
    }
    for(var i =0; i<=5; i++)
    {
        $("#lb"+i).children().removeClass("active");
    }
    for(var i =0; i<=5; i++)
    {
        $("#l"+i).children().removeClass("active");
    }
    for(var i =0; i<=5; i++)
    {
        $("#lt"+i).children().removeClass("active");
    }
}
/*******************************************************************/
/*  Debido a que es necesario una anidación para poder
    llevar a cabo la sincronizacion esta funcion permite el
    orden de los querys y que todo se ejecute sin problemas
    de sincronizacion gracias a la recursividadTabla               */
/*******************************************************************/
function recursividadTablaCliente(cliente,j)
{
        t = 0;
        rt = 0;
        r = 0;
        rb = 0;
        b = 0;
        lb = 0;
        l = 0;
        lt = 0;
        var Cliente = Parse.Object.extend("Cliente");
        var query = new Parse.Query(Cliente);
        query.equalTo("objectId",cliente[j]);
        query.find({
        success: function(results) {
            var clienteID = results[0];
            var Evaluacion = Parse.Object.extend("Evaluacion");
            var query = new Parse.Query(Evaluacion);
            query.equalTo("cliente",clienteID);
            query.find({
            success: function(results) {
                if(results.length>0)
                {
                    for (var i = 0; i < results.length; i++) {
                    var object = results[i];

                    //t+=object.get("physical");
                    t+=object.get("choice");
                    //rt+=object.get("choice");
                    rt+=object.get("posture");
                    //r+=object.get("posture");
                    r+=object.get("privacy");
                    //rb+=object.get("control");
                    rb+=object.get("presence");
                    //b+=object.get("presence");
                    b+=object.get("control");
                    lb+=object.get("emotional");
                    //l+=object.get("privacy");
                    l+=object.get("cognitive");
                    //lt+=object.get("cognitive");
                    lt+=object.get("physical");
                    }

                    var fct = (t/results.length).toFixed(1);
                    var fcrt = (rt/results.length).toFixed(1);
                    var fcr = (r/results.length).toFixed(1);
                    var fcrb = (rb/results.length).toFixed(1);
                    var fcb = (b/results.length).toFixed(1);
                    var fclb = (lb/results.length).toFixed(1);
                    var fcl = (l/results.length).toFixed(1);
                    var fclt = (lt/results.length).toFixed(1);

                    ct += t;
                    crt += rt;
                    cr += r;
                    crb += rb;
                    cb += b;
                    clb += lb;
                    cl += l;
                    clt += lt;
                    encuestas+=results.length;
                    var color = $("#"+cliente[j]).children().attr('id');
                    $("#"+cliente[j]).css("color",color);
                    obtenerValoresParaElCanvas(Math.round(t/results.length),Math.round(rt/results.length),Math.round(r/results.length),Math.round(rb/results.length),Math.round(b/results.length),Math.round(lb/results.length),Math.round(l/results.length),Math.round(lt/results.length),color);

                    tabla+="<tr><td style='border-right: solid #404041 1px;'>"+clienteID.get('nombre')+"</td><td class='tableTD'>"+fct+"</td><td class='tableTD'>"+fcrt+"</td><td class='tableTD'>"+fcr+"</td><td class='tableTD'>"+fcrb+"</td><td class='tableTD'>"+fcb+"</td><td class='tableTD'>"+fclb+"</td><td class='tableTD'>"+fcl+"</td><td class='tableTD'>"+fclt+"</td></tr>";

                    //tabla+="<tr><td>"+clienteID.get('nombre')+"</td><td>"+fct+"</td><td>"+fcrt+"</td><td>"+fcr+"</td><td>"+fcrb+"</td><td>"+fcb+"</td><td>"+fclb+"</td><td>"+fcl+"</td><td>"+fclt+"</td></tr>";
                }

                j++;
                if(j<cliente.length)
                {
                    recursividadTablaCliente(cliente,j);
                }
                else
                {
                    tabla+="</tbody></table>";
                    var pTotal=((ct+crt+cr+crb+cb+clb+cl+clt)/encuestas)/8;
                    ct = Math.round(ct/encuestas);
                    crt = Math.round(crt/encuestas);
                    cr = Math.round(cr/encuestas);
                    crb = Math.round(crb/encuestas);
                    cb = Math.round(cb/encuestas);
                    clb = Math.round(clb/encuestas);
                    cl = Math.round(cl/encuestas);
                    clt = Math.round(clt/encuestas);
                    $("#rt"+crt).children().addClass("active");
                    $("#t"+ct).children().addClass("active");
                    $("#r"+cr).children().addClass("active");
                    $("#rb"+crb).children().addClass("active");
                    $("#b"+cb).children().addClass("active");
                    $("#lb"+clb).children().addClass("active");
                    $("#l"+cl).children().addClass("active");
                    $("#lt"+clt).children().addClass("active");
                    $("#tabla").html(tabla);
                    $("#promedioTotal").html("PROMEDIO TOTAL: " +pTotal.toFixed(1));
		            $("#encuestas").html("ENCUESTAS: "+encuestas);
                    obtenerValoresParaElCanvas(ct,crt,cr,crb,cb,clb,cl,clt,0);

                }
            },
            error: function(error) {
                alert("Error: " + error.code + " " + error.message);
            }
            });
        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });
}



var ind="",clie="",per="",ar="",gen="";

/*******************************************************************/
/*  Name: seleccionarIndustriaDD;
    Function: cuando seleccionas una industria busca los clientes
            relacionados con esa industria y dependiendo de si
            hay valor de id como parametro lo agrega a un select o
            a un dropdown
    Input: id->string con el id del objeto(se puede omitir);
    Oputput: none;                                                 */
/*******************************************************************/

function  seleccionarIndustriaDD(id)
{
    //console.log("Entre a seleccionar industria");
    //$("#industriaDD").css("display", "none");
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
	var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0;
    if(id)
    {
        var info = id.split("*");
    }

    var Industria = Parse.Object.extend("Industria");
    var query = new Parse.Query(Industria);
    if(id)
    {
        query.equalTo('objectId', info[1]);
    }
    else{
    query.equalTo('objectId', $("#SelectIndustria").val());

    }
    query.find({
    success: function(results) {
        var object = results[0];
        if(id)
        {
            $("#iDD").children().find("p").html(object.get("nombre"));
            $("#cDD").children().find("p").html("CLIENTE");
            $("#pDD").children().find("p").html("PERIODO");
            $("#aDD").children().find("p").html("ÁREA");
            $("#gDD").children().find("p").html("GENERACIÓN");
            limpiarValoresDelCirculo();
            limpiarCanvas();
            ind=object.get("nombre");
            clie="";
            per="";
            ar="";
            gen="";
        }
        var Cliente = Parse.Object.extend("Cliente");
        var query = new Parse.Query(Cliente);
        query.equalTo('Industria',object)
        query.find({
        success: function(results) {
            var res="";
            for (var i = 0; i < results.length; i++) {
            var object = results[i];
            if(id)
            {
                res+="<a  id='"+object.get('nombre')+"*"+object.id+"' onClick='seleccionarClienteDD(this.id);'>"+object.get('nombre')+"</a>";
            }
            else
            {
                res+="<option value='"+object.id+"'>"+object.get('nombre')+"</option>";
            }

            }
            if(id)
            {
                $("#clienteDD").html(res);
                $("#cDD").css("display", "block");
                CrearHeaderAdmin();
            }
            else{
                res="<option value=''></option>"+res;
                $("#SelectCliente").html(res);
            }



        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });

    },
    error: function(error) {
        alert("Error: " + error.code + " " + error.message);
    }
    });
    if(id)
    {
        //mostrarIndustriaDD();
        OcultarTodo();

    }


}

/*******************************************************************/
/*  Name: seleccionarClienteDD;
    Function: Toma el valor de dropdown de cliente y dependiendo
            de dicho valor, busca las areas o generaciones ligadas.
    Input: id->string con el id;
    Oputput: none;                                                 */
/*******************************************************************/
function  seleccionarClienteDD(id)
{
    //$("#industriaDD").css("display", "none");
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
	var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0;
    if(id)
    {
        var info = id.split("*");
    }
    var cli;
    var Cliente = Parse.Object.extend("Cliente");
    var query = new Parse.Query(Cliente);
    if(id)
    {
        cli=info[1];
    }
    else{
        cli=$("#SelectIndustria").val();

    }
    query.get(cli,{
    success: function(results) {
        var object = results;
        if(id)
        {
            $("#cDD").children().find("p").html(object.get("nombre"));
            $("#pDD").children().find("p").html("PERIODO");
            $("#aDD").children().find("p").html("ÁREA");
            $("#gDD").children().find("p").html("GENERACIÓN");
            limpiarValoresDelCirculo();
            limpiarCanvas();
            clie=object.get("nombre");
            per="";
            ar="";
            gen="";
        }
        var Periodo = Parse.Object.extend("Periodo");
        var query = new Parse.Query(Periodo);
        query.equalTo('cliente',object)
        query.find({
        success: function(results) {
            var res="";
            for (var i = 0; i < results.length; i++) {
            var object = results[i];
            if(id)
            {
                res+="<a  id='"+object.get('nombre')+"*"+object.id+"' onClick='seleccionarPeriodoDD(this.id);'>"+object.get('nombre')+"</a>";
            }
            else
            {
                res+="<option value='"+object.id+"'>"+object.get('nombre')+"</option>";
            }

            }
            if(id)
            {
                $("#periodoDD").html(res);
                $("#pDD").css("display", "block");
                CrearHeaderAdmin();
            }
            else{
                res="<option value=''></option>"+res;
            }



        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });

    },
    error: function(error) {
        alert("Error: " + error.code + " " + error.message);
    }
    });
    if(id)
    {
        //mostrarClienteDD();
        OcultarTodo();
    }


}



/*******************************************************************/
/*  Name: seleccionarClienteAnalytics;
    Function: Toma los datos de un deteminado cliente y muestra sus
            detalles;
    Input: id->String con el id del cliente;
    Oputput: none;                                                 */
/*******************************************************************/
function  seleccionarClienteAnalytics(id,AG)
{
    //$("#industriaDD").css("display", "none");
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';

    if(id)
    {
        var info = id.split("*");
    }
    var cli;
    var Cliente = Parse.Object.extend("Cliente");
    var query = new Parse.Query(Cliente);
    if(id)
    {
        cli=info[1];
    }
    else{
        cli=$("#SelectIndustria").val();

    }
    query.get(cli,{
    success: function(results) {
        var object = results;
        if(id)
        {
            $("#cDD").children().find("p").html(object.get("nombre"));
            $("#headerAnalytics").html(object.get("nombre"));
            $("#pDD").children().find("p").html("PERIODO");
            $("#aDD").children().find("p").html("ÁREA");
            $("#gDD").children().find("p").html("GENERACIÓN");
            limpiarValoresDelCirculo();
            clie=object.get("nombre");
        }
        var Periodo = Parse.Object.extend("Periodo");
        var query = new Parse.Query(Periodo);
        query.equalTo('cliente',object)
        query.find({
        success: function(results) {
            var res="";
            for (var i = 0; i < results.length; i++) {
            var object = results[i];
            if(id)
            {
                res+="<a  id='"+object.id+"' onClick='mostrarAnalytics(this.id,"+AG+");'>"+object.get('nombre')+"</a>";
            }
            else
            {
                res+="<option value='"+object.id+"'>"+object.get('nombre')+"</option>";
            }

            }
            if(id)
            {
                $("#periodoDD").html(res);
                $("#pDD").css("display", "block");
                CrearHeaderAdmin();
            }
            else{
                res="<option value=''></option>"+res;
            }



        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });

    },
    error: function(error) {
        alert("Error: " + error.code + " " + error.message);
    }
    });
    if(id)
    {
        //mostrarClienteDD();
    }


}


/*******************************************************************/
/*  Name: mostrarAnalytics;
    Function: muestra las analytics por un periodo dado;
    Input: id->String con el id del periodo;
    Oputput: none;                                                 */
/*******************************************************************/
function mostrarAnalytics(id,AG)
{
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
    var Periodo = Parse.Object.extend("Periodo");
    var query = new Parse.Query(Periodo);
    query.get(id, {
        success: function(pe) {
            $("#headerAnalytics").html($("#headerAnalytics").html()+"/"+pe.get("nombre"));
            $("#pDD").children().find("p").html(pe.get("nombre"));
            var Evaluacion = Parse.Object.extend("Evaluacion");
            var query = new Parse.Query(Evaluacion);
            query.equalTo('periodo',pe);
            if(AG==1)
            {
                query.ascending("area");
            }
            else
            {
                query.ascending("generacion");
            }
            query.find({
            success:function(evaluaciones) {
                var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0;
                var contadorEvaluaciones=0;
                var contadorAreas=0;
                var res="";
                var nombre="";
                var nombreAnterior=""
                var listaNombres="";
                for(var i=0; i<evaluaciones.length;i++)
                {
                    var evaluacion=evaluaciones[i];
                    if(AG==1)
                    {
                        nombre=evaluacion.get("area").id;
                    }
                    else
                    {
                        nombre=evaluacion.get("generacion").id;

                    }
                    if(nombre!=nombreAnterior)
                    {
                        if(nombreAnterior!="")
                        {
                            listaNombres+=nombreAnterior+"*";
                            t/=contadorEvaluaciones;
                            b/=contadorEvaluaciones;
                            l/=contadorEvaluaciones;
                            r/=contadorEvaluaciones;
                            lt/=contadorEvaluaciones;
                            lb/=contadorEvaluaciones;
                            rt/=contadorEvaluaciones;
                            rb/=contadorEvaluaciones;



                            res+=map(t,rt,r,rb,b,lb,l,lt,nombreAnterior, contadorEvaluaciones);

                            //res+=nombreAnterior+" "+t+" "+rt+" "+r+" "+rb+" "+b+" "+lb+" "+l+" "+lt+"\n";
                        }
                        nombreAnterior=nombre;
                        contadorEvaluaciones=0;
                        contadorAreas++;
                        t=0;
                        b=0;
                        l=0;
                        r=0;
                        lt=0;
                        lb=0;
                        rt=0;
                        rb=0;
                    }
                    contadorEvaluaciones++;
                    t+=evaluacion.get("choice");
                    rt+=evaluacion.get("posture");
                    r+=evaluacion.get("privacy");
                    rb+=evaluacion.get("presence");
                    b+=evaluacion.get("control");
                    lb+=evaluacion.get("emotional");
                    l+=evaluacion.get("cognitive");
                    lt+=evaluacion.get("physical");

                }
                t/=contadorEvaluaciones;
                b/=contadorEvaluaciones;
                l/=contadorEvaluaciones;
                r/=contadorEvaluaciones;
                lt/=contadorEvaluaciones;
                lb/=contadorEvaluaciones;
                rt/=contadorEvaluaciones;
                rb/=contadorEvaluaciones;
                res+=map(t,rt,r,rb,b,lb,l,lt,nombreAnterior,contadorEvaluaciones);
                listaNombres+=nombreAnterior;
                if(listaNombres!="")
                {
                    var lista=listaNombres.split("*");
                    if(AG==1)
                    {
                        nombrarAreasRecursivo(lista,0);
                    }
                    else{
                        nombrarGeneracionesRecursivo(lista,0);

                    }
                }

                //res+=nombreAnterior+" "+t+" "+rt+" "+r+" "+rb+" "+b+" "+lb+" "+l+" "+lt;
                $("#circleAnalytics").html(res);
            },
            error: function(error) {
                alert("Error: " + error.code + " " + error.message);
            }
            });
        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });
        //mostrarPeriodoDD();

}

/*******************************************************************/
/*  Name: seleccionarPeriodoDD;
    Function: Toma el valor del periodo seleccionado en el dropdown
            y obtiene las areas y generaciones ligadas a el
    Input: id->String con el id del periodo
    Oputput: none;                                                 */
/*******************************************************************/
function  seleccionarPeriodoDD(id)
{
    //$("#clienteDD").css("display", "none");
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
	var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0;

    var info = id.split("*");

    var Periodo = Parse.Object.extend("Periodo");
    var query = new Parse.Query(Periodo);
    query.get(info[1],{
    success: function(results) {
        var object=results;
        var periodo = results;
        $("#pDD").children().find("p").html(object.get("nombre"));
        per=object.get("nombre");
        $("#aDD").children().find("p").html("ÁREA");
        $("#gDD").children().find("p").html("GENERACIÓN");
        limpiarValoresDelCirculo();
        limpiarCanvas();
        ar="";
        gen="";




        var Area = Parse.Object.extend("Area");
        var query = new Parse.Query(Area);
        query.equalTo('periodo',periodo);
        query.find({
        success: function(results) {
            var res="";
            for (var i = 0; i < results.length; i++) {
            var object = results[i];
            res+="<a  id='"+object.get('nombre')+"*"+object.id+"' onClick='seleccionarAreaDD(this.id);'>"+object.get('nombre')+"</a>";

            }
            $("#areaDD").html(res);
            $("#aDD").css("display", "block");
            var Generacion = Parse.Object.extend("Generacion");
            var query = new Parse.Query(Generacion);
            query.equalTo('periodo',periodo);
            query.find({
            success: function(results) {
                var res="";
                for (var i = 0; i < results.length; i++) {
                var object = results[i];
                res+="<a  id='"+object.get('nombre')+"*"+object.id+"' onClick='seleccionarGeneracionDD(this.id);'>"+object.get('nombre')+"</a>";

                }
                $("#generacionDD").html(res);
                $("#gDD").css("display", "block");
                CrearHeaderAdmin();



            },
            error: function(error) {
                alert("Error: " + error.code + " " + error.message);
            }
            });


        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });

    },
    error: function(error) {
        alert("Error: " + error.code + " " + error.message);
    }
    });
    //mostrarPeriodoDD();
    OcultarTodo();
}


/*******************************************************************/
/*  Name: seleccionarAreaDD;
    Function: Toma todas las encuestas del area correspondiente
            y las muestra graficamente
    Input: id->String con el id del area;
    Oputput: none;                                                 */
/*******************************************************************/
function seleccionarAreaDD(id)
{
    //$("#clienteDD").css("display", "none");
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
	var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0;

    var info = id.split("*");

    var Area = Parse.Object.extend("Area");
    var query = new Parse.Query(Area);
    query.get(info[1],{
    success: function(area) {
        $("#aDD").children().find("p").html(area.get("nombre"));
        ar=area.get("nombre");
        $("#gDD").children().find("p").html("GENERACIÓN");
        gen="";
        var Evaluacion = Parse.Object.extend("Evaluacion");
        var query = new Parse.Query(Evaluacion);
        query.equalTo('area',area);
        query.find({
        success: function(results) {
            var res="";
            for (var i = 0; i < results.length; i++) {
            var object = results[i];

            //t+=object.get("physical");
            t+=object.get("choice");
            //rt+=object.get("choice");
            rt+=object.get("posture");
            //r+=object.get("posture");
            r+=object.get("privacy");
            //rb+=object.get("control");
            rb+=object.get("presence");
            //b+=object.get("presence");
            b+=object.get("control");
            lb+=object.get("emotional");
            //l+=object.get("privacy");
            l+=object.get("cognitive");
            //lt+=object.get("cognitive");
            lt+=object.get("physical");

            }
            limpiarValoresDelCirculo();
            limpiarCanvas();

            var ct = Math.round(t/results.length);
            $("#t"+ct).children().addClass("active");
            var crt = Math.round(rt/results.length);
            $("#rt"+crt).children().addClass("active");
            var cr = Math.round(r/results.length);
            $("#r"+cr).children().addClass("active");
            var crb = Math.round(rb/results.length);
            $("#rb"+crb).children().addClass("active");
            var cb = Math.round(b/results.length);
            $("#b"+cb).children().addClass("active");
            var clb = Math.round(lb/results.length);
            $("#lb"+clb).children().addClass("active");
            var cl = Math.round(l/results.length);
            $("#l"+cl).children().addClass("active");
            var clt = Math.round(lt/results.length);
            $("#lt"+clt).children().addClass("active");


            obtenerValoresParaElCanvas(ct,crt,cr,crb,cb,clb,cl,clt,0);

            var pTotal=(((t+rt+r+rb+b+lb+l+lt)/8)/results.length).toFixed(1);
            CrearHeaderAdmin();
            $("#promedioTotal").html("PROMEDIO TOTAL: " +pTotal);
		    $("#encuestas").html("ENCUESTAS: "+results.length);


        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });

    },
    error: function(error) {
        alert("Error: " + error.code + " " + error.message);
    }
    });
    //mostrarAreaDD();
    OcultarTodo();

}


/*******************************************************************/
/*  Name: seleccionarGeneracionDD;
    Function: Toma las encuestas ligadas a la generacion
            correspondiente y las despliega graficamente.
    Input: id->String con el id de la generacion;
    Oputput: none;                                                 */
/*******************************************************************/
function  seleccionarGeneracionDD(id)
{
    //$("#clienteDD").css("display", "none");
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
	var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0;

    var info = id.split("*");

    var Generacion = Parse.Object.extend("Generacion");
    var query = new Parse.Query(Generacion);
    query.get(info[1],{
    success: function(generacion) {
        $("#gDD").children().find("p").html(generacion.get("nombre"));
        gen=generacion.get("nombre");
        $("#aDD").children().find("p").html("ÁREA");
        ar="";
        var Evaluacion = Parse.Object.extend("Evaluacion");
        var query = new Parse.Query(Evaluacion);
        query.equalTo('generacion',generacion);
        query.find({
        success: function(results) {
            var res="";
            for (var i = 0; i < results.length; i++) {
            var object = results[i];

            //t+=object.get("physical");
            t+=object.get("choice");
            //rt+=object.get("choice");
            rt+=object.get("posture");
            //r+=object.get("posture");
            r+=object.get("privacy");
            //rb+=object.get("control");
            rb+=object.get("presence");
            //b+=object.get("presence");
            b+=object.get("control");
            lb+=object.get("emotional");
            //l+=object.get("privacy");
            l+=object.get("cognitive");
            //lt+=object.get("cognitive");
            lt+=object.get("physical");

            }
            limpiarValoresDelCirculo();
            limpiarCanvas();
            var ct = Math.round(t/results.length);
            $("#t"+ct).children().addClass("active");
            var crt = Math.round(rt/results.length);
            $("#rt"+crt).children().addClass("active");
            var cr = Math.round(r/results.length);
            $("#r"+cr).children().addClass("active");
            var crb = Math.round(rb/results.length);
            $("#rb"+crb).children().addClass("active");
            var cb = Math.round(b/results.length);
            $("#b"+cb).children().addClass("active");
            var clb = Math.round(lb/results.length);
            $("#lb"+clb).children().addClass("active");
            var cl = Math.round(l/results.length);
            $("#l"+cl).children().addClass("active");
            var clt = Math.round(lt/results.length);
            $("#lt"+clt).children().addClass("active");
            obtenerValoresParaElCanvas(ct,crt,cr,crb,cb,clb,cl,clt,0);
            CrearHeaderAdmin();


        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });

    },
    error: function(error) {
        alert("Error: " + error.code + " " + error.message);
    }
    });
    //mostrarGeneracionDD();
    OcultarTodo();

}

/*******************************************************************/
/*  Name: CrearHeaderAdmin;
    Function: Le da diseño a la pagina admini.html, va cambiando
            el header dependiendo la opciones seleccionadas
    Input: id->String con el id del cliente;
    Oputput: none;                                                 */
/*******************************************************************/
function CrearHeaderAdmin()
{
    var header="";
    if(ind=="")
    {
        header="INDUSTRIA/";
    }
    else
    {
        if(clie=="")
        {
            header=ind+"/CLIENTE";
        }
        else
        {
            if(per=="")
            {
                header=ind+"/"+clie+"/PERIODO";
            }
            else
            {
                if(ar=="")
                {
                    if(gen=="")
                    {
                        header=ind+"/"+clie+"/"+per+"/";
                    }
                    else
                    {
                        header=ind+"/"+clie+"/"+per+"/"+gen;
                    }
                }
                else
                {
                    header=ind+"/"+clie+"/"+per+"/"+ar;
                }
            }
        }
    }
    $("#headerAdmin").html(header);

}



/*******************************************************************/
/*  Las siguientes funciones sólo sirven para hacer visibles
    u ocultar ciertas partes de la pagina, el nombre de la
función muestra cual es la parte que se ve afectada                */
/*******************************************************************/

function OcultarTodo()
{
    $("#industriaDD").css('display','none');
    $("#clienteDD").css('display','none');
    $("#periodoDD").css('display','none');
    $("#areaDD").css('display','none');
    $("#generacionDD").css('display','none');
    $("#options").css('display','none');
    $("#antiguedad").css('display','none');
    $("#areaD").css('display','none');
    $("#generacionD").css('display','none');
    $("#industriaD").css('display','none');
      $("#results").css('display','none');
      $("#industriaR").css('display','none');
      $("#empresaR").css('display','none');
      $("#areaR").css('display','none');
      $("#antiguedadR").css('display','none');
      $('#generacionR').css('display','none')
}
function mostrarIndustriaDD()
{
    if($("#industriaDD").css('display')=="none")
    {
        $("#industriaDD").css('display','block');
    }
    else{
        //$("#industriaDD").css('display','none');
    }
}

function mostrarResultados(){
  if($("#results").css('display')=="none")
  {
      $("#results").css('display','block');
  }
  else{
      //$("#industriaDD").css('display','none');
  }
}

function mostrarOpciones()
{
    if($("#options").css('display')=="none")
    {
        $("#options").css('display','block');
    }
    else{
        //$("#industriaDD").css('display','none');
    }
}

function mostrarAreas()
{
    if($("#areaD").css('display')=="none")
    {
        $("#areaD").css('display','block');
    }
    else{
        //$("#industriaDD").css('display','none');
    }
}

function mostrarIndustrias()
{
    if($("#industriaD").css('display')=="none")
    {
        $("#industriaD").css('display','block');
    }
    else{
        //$("#industriaDD").css('display','none');
    }
}

function mostrarIndustriasRes()
{
  //OcultarTodo();
    if($("#industriaR").css('display')=="none")
    {
        $("#industriaR").css('display','block');
    }
    else{
        $("#industriaR").css('display','none');
    }
}

function mostrarEmpresasRes()
{
    if($("#empresaR").css('display')=="none")
    {
        $("#empresaR").css('display','block');
    }
    else{
        $("#empresaR").css('display','none');
    }
}

function mostrarAreasRes()
{
    if($("#areaR").css('display')=="none")
    {
        $("#areaR").css('display','block');
    }
    else{
        $("#areaR").css('display','none');
    }
}


function mostrarAntiguedadRes()
{
    if($("#antiguedadR").css('display')=="none")
    {
        $("#antiguedadR").css('display','block');
    }
    else{
        $("#antiguedadR").css('display','none');
    }
}

function mostrarGeneracionesRes()
{
    if($("#generacionR").css('display')=="none")
    {
        $("#generacionR").css('display','block');
    }
    else{
        $("#generacionR").css('display','none');
    }
}


function mostrarAntiguedad()
{
    if($("#antiguedad").css('display')=="none")
    {
        $("#antiguedad").css('display','block');
    }
    else{
        $("#antiguedad").css('display','none');
    }
}

function mostrarGeneracion()
{
    if($("#generacionD").css('display')=="none")
    {
        $("#generacionD").css('display','block');
    }
    else{
        //$("#industriaDD").css('display','none');
    }
}

function mostrarClienteDD()
{
    if($("#clienteDD").css('display')=="none")
    {
        $("#clienteDD").css('display','block');
    }
    else{
        //$("#clienteDD").css('display','none');
    }
}

function mostrarPeriodoDD()
{
    if($("#periodoDD").css('display')=="none")
    {
        $("#periodoDD").css('display','block');
    }
    else{
        //$("#periodoDD").css('display','none');
    }
}



function mostrarAreaDD()
{
    if($("#areaDD").css('display')=="none")
    {
        $("#areaDD").css('display','block');
    }
    else{
        //$("#areaDD").css('display','none');
    }
}

function mostrarGeneracionDD()
{
    if($("#generacionDD").css('display')=="none")
    {
        $("#generacionDD").css('display','block');
    }
    else{
        //$("#generacionDD").css('display','none');
    }
}

function OcultarPopUp(tipo)
{

$('input[type=checkbox]').prop('checked', false);
$('#link').val("");
$('#newCliente').val("");
    OcultarTodo();
$('#linkWell').css('display','none');

  if(tipo == "wellbeing"){
$("#genPop").hide();
$("#agregaArea").hide();
$("#venPop").hide();
$("#cuestionario").hide();
$("#encWell").show();


    if($("#popup").css('display')=="none")
    {
        $("#popup").css('display','block');
          $('#linkWell').css('display','block');
    }
    else{
        $("#popup").css('display','none');
    }

  }else{
    $("#genPop").show();
    $("#areasPop").show();
    $("#indPop").show();
    $("#clPop").show();
    $("#venPop").show();
    $("#cuestionario").show();
    $("#encWell").hide();
    if($("#popup").css('display')=="none")
    {
        $("#popup").css('display','block');
          $("#crearLink").css('display','block');
    }
    else{
        $("#popup").css('display','none');
    }
  }

}



/*******************************************************************/
/*  Name: CrearLink;
    Function: Tomando los datos de entrada que puso el usuario en el
            popup se crea un link que permitira ir a la encuesta.
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function CrearLink(tipoEncuesta)
{

    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
    var areas="";
    $("input[name='area']:checked").each ( function() {
   			areas += $(this).val() + "*";
        //chkId = chkId.slice(0, -1);
 	  });

       if($("#areaOtro").val()!="")
       {
           areas+=$("#areaOtro").val()+"*";
       }

    var generacion="";
    $("input[name='Generacion']:checked").each ( function() {
   			generacion += $(this).val() + "*";
        //chkId = chkId.slice(0, -1);
 	  });

     if(areas==""||generacion==""||$("#Periodo").val()=="")
     {
         alert("No se han elegido areas, generaciones o Perido");
     }
    else
    {
        if($("#SelectVendedor").val()!="")
        {
            if($("#SelectIndustria").val()!="")
            {
                if($("#SelectCliente").val()!="")
                {
                    var Industria = Parse.Object.extend("Industria");
                    var query = new Parse.Query(Industria);
                    query.get($("#SelectIndustria").val(), {
                    success: function(industria) {
                        var Cliente = Parse.Object.extend("Cliente");
                        var query = new Parse.Query(Cliente);
                        query.get($("#SelectCliente").val(), {
                        success: function(cliente) {
                            var Periodo = Parse.Object.extend("Periodo");
                            var periodo = new Periodo();
                            periodo.set("nombre", $("#Periodo").val());
                            periodo.set("cliente",cliente);
                            periodo.save(null, {
                            success: function(periodo) {
                                var areasDivididas=areas.split("*");
                                var generacionDivididas =generacion.split("*");
                                recursividadAgregarAreas(areasDivididas,0,cliente,generacionDivididas,industria,periodo,tipoEncuesta);

                            },
                            error: function(periodo, error) {
                                alert('Failed to create new object, with error code: ' + error.message);
                            }
                            });
                        },
                        error: function(cliente, error) {
                            alert('Failed to create new object, with error code: ' + error.message);
                        }
                        });
                    },
                    error: function(industria, error) {
                        alert('Failed to create new object, with error code: ' + error.message);
                    }
                    });
                }
                else
                {
                    if($("#NuevoCliente").val()!="")
                    {
                        var Industria = Parse.Object.extend("Industria");
                        var query = new Parse.Query(Industria);
                        query.get($("#SelectIndustria").val(), {
                        success: function(industria) {
                            var Vendedor = Parse.Object.extend("Vendedor");
                            var query = new Parse.Query(Vendedor);
                            query.get($("#SelectVendedor").val(), {
                            success: function(vendedor) {

                                var Cliente = Parse.Object.extend("Cliente");
                                var cliente = new Cliente();
                                cliente.set("nombre", $("#NuevoCliente").val());
                                cliente.set("Industria",industria);
                                cliente.set("Vendedor",vendedor);
                                cliente.save(null, {
                                success: function(cliente) {
                                    var Periodo = Parse.Object.extend("Periodo");
                                    var periodo = new Periodo();
                                    periodo.set("nombre", $("#Periodo").val());
                                    periodo.set("cliente",cliente);
                                    periodo.save(null, {
                                    success: function(periodo) {
                                        var areasDivididas=areas.split("*");
                                        var generacionDivididas =generacion.split("*");
                                        recursividadAgregarAreas(areasDivididas,0,cliente,generacionDivididas,industria,periodo,tipoEncuesta);

                                    },
                                    error: function(periodo, error) {
                                        alert('Failed to create new object, with error code: ' + error.message);
                                    }
                                    });
                                },
                                error: function(cliente, error) {
                                    alert('Failed to create new object, with error code: ' + error.message);
                                }
                                });

                            },
                            error: function(industria, error) {
                                alert('Failed to create new object, with error code: ' + error.message);
                            }
                            });

                        },
                        error: function(industria, error) {
                            alert('Failed to create new object, with error code: ' + error.message);
                        }
                        });
                    }
                    else
                    {
                        alert("No se ha seleccionado cliente.");
                    }
                }
            }
            else
            {
                if($("#NuevaIndustria").val()!="")
                {
                    if($("#NuevoCliente").val()!="")
                    {
                        var Industria = Parse.Object.extend("Industria");
                        var industria = new Industria();
                        industria.set("nombre", $("#NuevaIndustria").val());
                        industria.save(null, {
                        success: function(industria) {
                            var Vendedor = Parse.Object.extend("Vendedor");
                            var query = new Parse.Query(Vendedor);
                            query.get($("#SelectVendedor").val(), {
                            success: function(vendedor) {
                                var Cliente = Parse.Object.extend("Cliente");
                                var cliente = new Cliente();
                                cliente.set("nombre", $("#NuevoCliente").val());
                                cliente.set("Industria",industria);
                                cliente.set("Vendedor",vendedor);
                                cliente.save(null, {
                                success: function(cliente) {

                                    var Periodo = Parse.Object.extend("Periodo");
                                    var periodo = new Periodo();
                                    periodo.set("nombre", $("#Periodo").val());
                                    periodo.set("cliente",cliente);
                                    periodo.save(null, {
                                    success: function(periodo) {
                                        var areasDivididas=areas.split("*");
                                        var generacionDivididas =generacion.split("*");
                                        recursividadAgregarAreas(areasDivididas,0,cliente,generacionDivididas,industria,periodo,tipoEncuesta);

                                    },
                                    error: function(periodo, error) {
                                        alert('Failed to create new object, with error code: ' + error.message);
                                    }
                                    });
                                },
                                error: function(cliente, error) {
                                    alert('Failed to create new object, with error code: ' + error.message);
                                }
                                });

                            },
                            error: function(industria, error) {
                                alert('Failed to create new object, with error code: ' + error.message);
                            }
                            });
                        },
                        error: function(industria, error) {
                            alert('Failed to create new object, with error code: ' + error.message);
                        }
                        });
                    }
                    else
                    {
                        alert("No se ha seleccionado cliente.");
                    }
                }
                else
                {
                    alert("No se ha seleccionado industria.");
                }
            }
        }
        else
        {
            alert("No se ha seleccionado vendedor.");
        }
    }

}

/*******************************************************************/
/*  Las siguientes funciones son necesarias para poder
    insertar las areas y las generaciones que sean solicitado
    para cada cliente y trabajan de forma recursiva y dependiente
    ocupan la lista de ares, y de generaciones, un pointer j
    el cliente la industria y el periodo                           */
/*******************************************************************/
function recursividadAgregarAreas(areasDivididas,j,cliente,generacionDivididas,industria,periodo,tipoEncuesta)
{
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
    var Area = Parse.Object.extend("Area");
    var area = new Area();

    area.set("nombre",areasDivididas[j]);
    area.set("cliente",cliente);
    area.set("periodo",periodo);
    area.save(null,{
        success: function(area)
        {
            j++;
            if(j<areasDivididas.length-1)
            {
                recursividadAgregarAreas(areasDivididas,j,cliente,generacionDivididas,industria,periodo,tipoEncuesta);
            }
            else
            {
                recursividadAgregarGeneros(generacionDivididas,0,cliente,industria,periodo,tipoEncuesta);

            }

        },
        error: function(area, error)
        {

        }
    });
}

function recursividadAgregarGeneros(generacionDivididas,j,cliente,industria,periodo,tipoEncuesta)
{
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
    var Generacion = Parse.Object.extend("Generacion");
    var generacion = new Generacion();
    generacion.set("nombre",generacionDivididas[j]);
    generacion.set("cliente",cliente);
    generacion.set("periodo",periodo);
    generacion.save(null,{
        success: function(area)
        {
            j++;
            if(j<generacionDivididas.length-1)
            {
                recursividadAgregarGeneros(generacionDivididas,j,cliente,industria,periodo,tipoEncuesta);
            }
            else
            {
              if(tipoEncuesta == 'vieja'){
                  $("#link").val("https://hugrios.bitbucket.io/index.html?Industria="+industria.id+"&Cliente="+cliente.id+"&Periodo="+periodo.id);
              }else{
                $("#link").val("https://hugrios.bitbucket.io/inicio.html?Industria="+industria.id+"&Cliente="+cliente.id+"&Periodo="+periodo.id);
              }

            }

        },
        error: function(area, error)
        {

        }
    });
}


/*******************************************************************/
/*  Name: Emtrar;
    Function: Si el usuario es valido permite entrar al sistema
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/

function entrar()
{
    Parse.initialize("steelcaseCirclesAppId");
    Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
    Parse.User.logIn($("#user").val(), $("#password").val(), {
        success: function(user) {
            //Parse.User.logOut().then(() => {
            window.location="admini.html?U="+$("#user").val()+"&P="+$("#password").val();
            //});


    },
    error: function(user, error) {
        alert("Usuario o contraseña inexistente");
    }
    });
}

/*******************************************************************/
/*  Name: irA;
    Function: Te permite navegar por el sitio, ojo sirve por los
            nombre de los archivos html actuales, en caso de ser
            cambiados de debe cambiar el id de los botones de
            navegacion.
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/

function irA(id)
{
    if(id=="login")
    {
        window.location=id+".html";
    }
    else
    {
        window.location=id+".html?U="+get("U")+"&P="+get("P");

    }
}

/*******************************************************************/
/*  Name: map;
    Function: Crea el grafico circular donde se puede visualizar
            la informacion de cierta area.
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function map(t,rt,r,rb,b,lb,l,lt, id,contadorEvaluaciones)
{
    var res="";
    res+="<div id ='conti'>";
    res+="	<p style='color: #568EC8; margin: 0 39%;' id='"+id+"'>GEN Z</p>";
    res+="	</br> Respuestas: "+contadorEvaluaciones;
    res+="   <div id='caja'>";
    res+="       <div class ='asterisco'>";
    if(Math.round(b)==1)
    {
        res+="           <a><div class='mapita active'><p>1</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>1</p></div></a>";
    }
    if(Math.round(b)==2)
    {
        res+="           <a><div class='mapita active'><p>2</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>2</p></div></a>";
    }
    if(Math.round(b)==3)
    {
        res+="           <a><div class='mapita active'><p>3</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>3</p></div></a>";
    }
    if(Math.round(b)==4)
    {
        res+="           <a><div class='mapita active'><p>4</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>4</p></div></a>";
    }
    if(Math.round(b)==5)
    {
        res+="           <a><div class='mapita active'><p>5</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>5</p></div></a>";
    }

    res+="       </div>";

    res+="       <div class ='asterisco'>";
    if(Math.round(lb)==1)
    {
        res+="           <a><div class='mapita active'><p>1</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>1</p></div></a>";
    }
    if(Math.round(lb)==2)
    {
        res+="           <a><div class='mapita active'><p>2</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>2</p></div></a>";
    }
    if(Math.round(lb)==3)
    {
        res+="           <a><div class='mapita active'><p>3</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>3</p></div></a>";
    }
    if(Math.round(lb)==4)
    {
        res+="           <a><div class='mapita active'><p>4</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>4</p></div></a>";
    }
    if(Math.round(lb)==5)
    {
        res+="           <a><div class='mapita active'><p>5</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>5</p></div></a>";
    }

    res+="       </div>";
    res+="       <div class ='asterisco'>";
    if(Math.round(rb)==1)
    {
        res+="           <a><div class='mapita active'><p>1</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>1</p></div></a>";
    }
    if(Math.round(rb)==2)
    {
        res+="           <a><div class='mapita active'><p>2</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>2</p></div></a>";
    }
    if(Math.round(rb)==3)
    {
        res+="           <a><div class='mapita active'><p>3</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>3</p></div></a>";
    }
    if(Math.round(rb)==4)
    {
        res+="           <a><div class='mapita active'><p>4</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>4</p></div></a>";
    }
    if(Math.round(rb)==5)
    {
        res+="           <a><div class='mapita active'><p>5</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>5</p></div></a>";
    }

    res+="       </div>";
    res+="       <div class ='asterisco'>";
    if(Math.round(t)==1)
    {
        res+="           <a><div class='mapita active'><p>1</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>1</p></div></a>";
    }
    if(Math.round(t)==2)
    {
        res+="           <a><div class='mapita active'><p>2</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>2</p></div></a>";
    }
    if(Math.round(t)==3)
    {
        res+="           <a><div class='mapita active'><p>3</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>3</p></div></a>";
    }
    if(Math.round(t)==4)
    {
        res+="           <a><div class='mapita active'><p>4</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>4</p></div></a>";
    }
    if(Math.round(t)==5)
    {
        res+="           <a><div class='mapita active'><p>5</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>5</p></div></a>";
    }

    res+="       </div>";
    res+="       <div class ='asterisco'>";
    if(Math.round(l)==1)
    {
        res+="           <a><div class='mapita active'><p>1</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>1</p></div></a>";
    }
    if(Math.round(l)==2)
    {
        res+="           <a><div class='mapita active'><p>2</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>2</p></div></a>";
    }
    if(Math.round(l)==3)
    {
        res+="           <a><div class='mapita active'><p>3</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>3</p></div></a>";
    }
    if(Math.round(l)==4)
    {
        res+="           <a><div class='mapita active'><p>4</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>4</p></div></a>";
    }
    if(Math.round(l)==5)
    {
        res+="           <a><div class='mapita active'><p>5</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>5</p></div></a>";
    }

    res+="       </div>";
    res+="       <div class ='asterisco'>";
    if(Math.round(lt)==1)
    {
        res+="           <a><div class='mapita active'><p>1</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>1</p></div></a>";
    }
    if(Math.round(lt)==2)
    {
        res+="           <a><div class='mapita active'><p>2</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>2</p></div></a>";
    }
    if(Math.round(lt)==3)
    {
        res+="           <a><div class='mapita active'><p>3</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>3</p></div></a>";
    }
    if(Math.round(lt)==4)
    {
        res+="           <a><div class='mapita active'><p>4</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>4</p></div></a>";
    }
    if(Math.round(lt)==5)
    {
        res+="           <a><div class='mapita active'><p>5</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>5</p></div></a>";
    }

    res+="       </div>";
    res+="       <div class ='asterisco'>";
    if(Math.round(rt)==1)
    {
        res+="           <a><div class='mapita active'><p>1</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>1</p></div></a>";
    }
    if(Math.round(rt)==2)
    {
        res+="           <a><div class='mapita active'><p>2</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>2</p></div></a>";
    }
    if(Math.round(rt)==3)
    {
        res+="           <a><div class='mapita active'><p>3</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>3</p></div></a>";
    }
    if(Math.round(rt)==4)
    {
        res+="           <a><div class='mapita active'><p>4</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>4</p></div></a>";
    }
    if(Math.round(rt)==5)
    {
        res+="           <a><div class='mapita active'><p>5</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>5</p></div></a>";
    }

    res+="       </div>";
    res+="       <div class ='asterisco'>";
    if(Math.round(r)==1)
    {
        res+="           <a><div class='mapita active'><p>1</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>1</p></div></a>";
    }
    if(Math.round(r)==2)
    {
        res+="           <a><div class='mapita active'><p>2</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>2</p></div></a>";
    }
    if(Math.round(r)==3)
    {
        res+="           <a><div class='mapita active'><p>3</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>3</p></div></a>";
    }
    if(Math.round(r)==4)
    {
        res+="           <a><div class='mapita active'><p>4</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>4</p></div></a>";
    }
    if(Math.round(r)==5)
    {
        res+="           <a><div class='mapita active'><p>5</p></div></a>";
    }
    else
    {
        res+="           <a><div class='mapita'><p>5</p></div></a>";
    }
    res+="       </div>";
    res+="       <a><div id='zerito'><p>0</p></div></a>";
    res+="   </div>";
    res+="</div>";
    return res;
}

/*******************************************************************/
/*  Name: nombrarAreasRecursivo;
    Function: obtien los nombres de las areas usando su Id
    Input: listaNombres-> array con los id de las empresas
           j->int posicion en el arrreglo a buscar ;
    Oputput: none;                                                 */
/*******************************************************************/
function nombrarAreasRecursivo(listaNombres,j)
{
    if(j<listaNombres.length)
    {
        Parse.initialize("steelcaseCirclesAppId");
        Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';

        var Area = Parse.Object.extend("Area");
        var query = new Parse.Query(Area);
        query.get(listaNombres[j], {
        success: function(area) {
            $("#"+listaNombres[j]).html(area.get("nombre"));
            nombrarAreasRecursivo(listaNombres,++j);
        }
        });
    }


}

/*******************************************************************/
/*  Name: nombrarGeneracionesRecursivo;
    Function: obtien los nombres de las generaciones usando su Id
    Input: listaNombres-> array con los id de las empresas
           j->int posicion en el arrreglo a buscar ;
    Oputput: none;                                                 */
/*******************************************************************/
function nombrarGeneracionesRecursivo(listaNombres,j)
{
    if(j<listaNombres.length)
    {
        Parse.initialize("steelcaseCirclesAppId");
        Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';

        var Generacion = Parse.Object.extend("Generacion");
        var query = new Parse.Query(Generacion);
        query.get(listaNombres[j], {
        success: function(generacion) {
            $("#"+listaNombres[j]).html(generacion.get("nombre"));
            nombrarGeneracionesRecursivo(listaNombres,++j);
        }
        });
    }


}

/*******************************************************************/
/*  Name: hacerPDF;
    Function: Toma una captura de pantalla y la transforma en pdf
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function hacerPDF(pagina)
{
    //document.body
    //$("#caja").css("height",274);
    html2canvas(document.body, {
    onrendered: function(canvas) {
    //document.body.appendChild(canvas);
    //}});

    var imgData = canvas.toDataURL("image/jpg");
    var date = new Date();
    var day;
    if(date.getDate().length>1)
    {
        day=date.getDate();
    }
    else{
        day="0"+date.getDate();
    }
    var month
    if((date.getMonth()+1).toString().length>1)
    {
        month=(date.getMonth()+1);
    }
    else{
        month="0"+(date.getMonth()+1);
    }
    var fecha = day+"/"+month+"/"+date.getFullYear();
    var doc = new jsPDF('landscape')
    //var width = canvas.width/5;
    //var height = canvas.height/5;
        doc.addImage(imgData, 'JPG', 0, 0, 300, 210);
    doc.save(pagina+'_'+fecha+'.pdf');
    }
    });
}
/*******************************************************************/
/*  Name: comprobarUsuario;
    Function: Comprueba que el usuario se valido en caso de
            intentar acceder a alguna pagina del sitio de forma
            no legal, en caso de que falte la
            informacion bota al usuario del sistema.
    Input: none;
    Oputput: none;                                                 */
/*******************************************************************/
function comprobarUsuario()
{
    Parse.User.logIn(get("U"), get("P"), {
			success: function(user) {
				/*Parse.User.logOut().then(() => {

				});*/


		},
		error: function(user, error) {
                window.location="login.html";
		}
		});
}


var month_name = function(dt){
mlist = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "December" ];
  return mlist[dt.getMonth()];
}


function SeleccionarVendedor()
{
    //var res="<h2 style='font-size: 32px;'>Clientes:</h1>";
    var listacliente="";
    var Vendedor = Parse.Object.extend("Vendedor");
    var query = new Parse.Query(Vendedor);
    query.get($("#SelectVendedor").val(), {
    success: function(vendedor)  {
        var Cliente = Parse.Object.extend("Cliente");
        var query = new Parse.Query(Cliente);
        query.equalTo("Vendedor",vendedor);
        query.find({
            success:function(clientes) {
                for(var i =0; i<clientes.length;i++)
                {
                    var cliente=clientes[i];
                    listacliente+= cliente.id + "*";
                    //res+="<h2>"+cliente.get("nombre")+"</h2>";
                }
                //$("#clientesDelVendedor").html(res);
                mostrarDatosCliente(listacliente);
            }
        });

    },
    error: function(periodo, error) {
        alert('Failed to create new object, with error code: ' + error.message);
    }
    });
}







/*function actualizarCanvas()
{

    var c = $("#myCanvas");
    var ctx = c.getContext("2d");
    ctx.moveTo(0,0);
    ctx.lineTo(200,100);
    ctx.stroke();
}*/
