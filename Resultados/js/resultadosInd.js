var general=0;
var grlIndustria=0;
var totalEncuestas = 0;


Parse.initialize("steelcaseCirclesAppId");
Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';





$(document).ready(function(){
  Parse.initialize("steelcaseCirclesAppId");
  Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
  var url = window.location.search;
  var elige = url.includes('Industria');
  var id = url.substring(url.lastIndexOf('=')+1)
  if(elige == true){
  //  IndresEmocional();
    $("#totalGlobal").css('display','none');
    $("#regresa").show();
    var query = new Parse.Query('indWell');
        query.equalTo('objectId', id);
        query.find({
          success: function(res){
            string = res[0].get("Nombre");
            industria = string.toUpperCase();
            $("#sector").html("SECTOR "+industria);
          }
        })
  }else{
    //resEmocional();
    $("#totalIndustria").css('display','none');
    console.log("entra else")

  }

});




















//Resultados Por Industria
let IndpromiseAmarillo = new Promise((resolve, reject) =>{
var url = window.location.search;
var id = url.substring(url.indexOf("=")+1)
	var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0,eb=0;
	var pTotal=0;

  var Industria = Parse.Object.extend("indWell");
  var industria = new Industria();
      industria.id = id;

		var Evaluacion = Parse.Object.extend("Wellbeing");
    var query = new Parse.Query(Evaluacion);
    query.equalTo('industria', industria);
    query.find({
    success: function(results) {
        for (var i = 0; i < results.length; i++) {
        var object = results[i];

		//t+=object.get("physical");
		t+=object.get("postura");
		//rt+=object.get("choice");
		rt+=object.get("movimiento");
		//r+=object.get("posture");
		r+=object.get("temperatura");
		//rb+=object.get("control");
		rb+=object.get("peso");
		//b+=object.get("presence");
		b+=object.get("ergonomia");
		lb+=object.get("jornada");
		//l+=object.get("privacy");
		l+=object.get("dieta");
		//lt+=object.get("cognitive");
		lt+=object.get("luz");

		eb+=object.get("contactoexterior");
        }

		var fct = t/results.length;
		var fcrt = rt/results.length;
		var fcr = r/results.length;
		var fcrb = rb/results.length;
		var fcb = b/results.length;
		var fclb = lb/results.length;
		var fcl = l/results.length;
		var fclt = lt/results.length;
		var fceb = eb/results.length;
		pTotal=((fct+fcrt+fcr+fcrb+fcb+fclb+fcl+fclt+fceb)/9).toFixed(1);
		fct = (t/results.length).toFixed(1);
		fcrt = (rt/results.length).toFixed(1);
		fcr = (r/results.length).toFixed(1);
		fcrb = (rb/results.length).toFixed(1);
		fcb = (b/results.length).toFixed(1);
		fclb = (lb/results.length).toFixed(1);
		fcl = (l/results.length).toFixed(1);
		fclt = (lt/results.length).toFixed(1);
		fceb = (eb/results.length).toFixed(1);



    var ctx = document.getElementById("myChartYellow");
    var myChart = new Chart(ctx, {
      type: 'radar',
      data: {
        labels: [".", ".", ".", ".", ".", ".", ".", ".", "."],
        datasets: [{
          backgroundColor: "rgba(253, 99, 68, 0.81)",
          borderColor: "rgba(253, 86, 53, 1)",
          data: [fct, fcrt, fcr, fcrb, fcb, fclb, fcl, fclt, fceb]
        }]
      },
      options: {
       legend: { display: false },
       scale: {
     ticks: {
         beginAtZero: true,
         max: 5
     }
      }
    }
    });
		var number= parseFloat(pTotal);
		grlIndustria+=number;
		$("#promedioTotal").html(pTotal);

    resolve(number);
    },
    error: function(error) {
        alert("Error: " + error.code + " " + error.message);
    }
    });
});



let IndresCognitivo = new Promise((resolve,reject) =>{


  IndpromiseAmarillo.then((response) =>{
    var url = window.location.search;
    var id = url.substring(url.indexOf("=")+1)
    //var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0,eb=0;
    var Industria = Parse.Object.extend("indWell");
    var industria = new Industria();
        industria.id = id;
    //comprobarUsuario();
      var Evaluacion = Parse.Object.extend("WellCognitivo");
      var query = new Parse.Query(Evaluacion);
      query.equalTo('industria', industria);
      query.find({
      success: function(results) {
        var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0,eb=0;

          for (var i = 0; i < results.length; i++) {
          var object = results[i];
      //t+=object.get("physical");
      t+=object.get("privAcustica");
      //rt+=object.get("choice");
      rt+=object.get("privVisual");
      //r+=object.get("posture");
      r+=object.get("estres");
      //rb+=object.get("control");
      rb+=object.get("anonEstrategico");
      //b+=object.get("presence");
      b+=object.get("expSelectiva");
      lb+=object.get("privTerritorial");
      //l+=object.get("privacy");
      l+=object.get("bloqueEst");
      //lt+=object.get("cognitive");
      lt+=object.get("confidencialidad");

      eb+=object.get("revitalizacion");
          }

      var fct = t/results.length;
      var fcrt = rt/results.length;
      var fcr = r/results.length;
      var fcrb = rb/results.length;
      var fcb = b/results.length;
      var fclb = lb/results.length;
      var fcl = l/results.length;
      var fclt = lt/results.length;
      var fceb = eb/results.length;
      var pTotal=((fct+fcrt+fcr+fcrb+fcb+fclb+fcl+fclt+fceb)/9).toFixed(1);
      fct = (t/results.length).toFixed(1);
      fcrt = (rt/results.length).toFixed(1);
      fcr = (r/results.length).toFixed(1);
      fcrb = (rb/results.length).toFixed(1);
      fcb = (b/results.length).toFixed(1);
      fclb = (lb/results.length).toFixed(1);
      fcl = (l/results.length).toFixed(1);
      fclt = (lt/results.length).toFixed(1);
      fceb = (eb/results.length).toFixed(1);


      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
        type: 'radar',
        data: {
          labels: [".", ".", ".", ".", ".", ".", ".", ".", "."],
          datasets: [{
            backgroundColor: "rgba(253, 99, 68, 0.81)",
            borderColor: "rgba(253, 86, 53, 1)",
            data: [fct, fcrt, fcr, fcrb, fcb, fclb, fcl, fclt, fceb]
          }]
        },
        options: {
         legend: { display: false },
         scale: {
       ticks: {
           beginAtZero: true,
           max: 5
       }
        }
      }
      });


      //var pTotal=((fct+fcrt+fcr+fcrb+fcb+fclb+fcl+fclt)/8).toFixed(1);
      var total2= parseFloat(pTotal);
      var t2 = total2+response;
        resolve(t2);
        if (pTotal == 'NaN'){
         $("#promedioTotalAzul").html(0);
        }else{
          $("#promedioTotalAzul").html(pTotal);
          $("#pgeneral").html(final);
        }


      //$("#promedioTotalAzul").html(pTotal);
      },
      error: function(error) {
          alert("Error: " + error.code + " " + error.message);
      }
      });
  })
});


function IndresEmocional(){

    IndresCognitivo.then((response) =>{
      var url = window.location.search;
      var id = url.substring(url.indexOf("=")+1)
      Parse.initialize("steelcaseCirclesAppId");
        Parse.serverURL = 'https://steelcase-circles.herokuapp.com/parse';
      var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0,eb=0;
      var Industria = Parse.Object.extend("indWell");
      var industria = new Industria();
          industria.id = id;
        var Evaluacion = Parse.Object.extend("WellEmocional");
        var query = new Parse.Query(Evaluacion);
        query.equalTo('industria', industria);
        query.find({
        success: function(results) {

          //var t=0,b=0,l=0,r=0,lt=0,lb=0,rt=0,rb=0,eb=0;
            for (var i = 0; i < results.length; i++) {
            var object = results[i];

        //t+=object.get("physical");
        t+=object.get("pertenencia");
        //rt+=object.get("choice");
        rt+=object.get("conectarseOtros");
        //r+=object.get("posture");
        r+=object.get("interaccionSocial");
        //rb+=object.get("control");
        rb+=object.get("confianza");
        //b+=object.get("presence");
        b+=object.get("inovacion");
        lb+=object.get("trabajoEquipo");
        //l+=object.get("privacy");
        l+=object.get("resProblemas");
        //lt+=object.get("cognitive");
        lt+=object.get("pertenencia");

        eb+=object.get("proposito");
            }

        var fct = t/results.length;
        var fcrt = rt/results.length;
        var fcr = r/results.length;
        var fcrb = rb/results.length;
        var fcb = b/results.length;
        var fclb = lb/results.length;
        var fcl = l/results.length;
        var fclt = lt/results.length;
        var fceb = eb/results.length;
        var pTotal=((fct+fcrt+fcr+fcrb+fcb+fclb+fcl+fclt+fceb)/9).toFixed(1);
        fct = (t/results.length).toFixed(1);
        fcrt = (rt/results.length).toFixed(1);
        fcr = (r/results.length).toFixed(1);
        fcrb = (rb/results.length).toFixed(1);
        fcb = (b/results.length).toFixed(1);
        fclb = (lb/results.length).toFixed(1);
        fcl = (l/results.length).toFixed(1);
        fclt = (lt/results.length).toFixed(1);
        fceb = (eb/results.length).toFixed(1);




        var ctx = document.getElementById("myChartGreen");
        var myChart = new Chart(ctx, {
          type: 'radar',
          data: {
            labels: [".", ".", ".", ".", ".", ".", ".", ".", "."],
            datasets: [{
              backgroundColor: "rgba(253, 99, 68, 0.81)",
              borderColor: "rgba(253, 86, 53, 1)",
              data: [fct, fcrt, fcr, fcrb, fcb, fclb, fcl, fclt, fceb]
            }]
          },
          options: {
           legend: { display: false },
           scale: {
         ticks: {
             beginAtZero: true,
             max: 5
         }
          }
        }
        });



        //var pTotal=((fct+fcrt+fcr+fcrb+fcb+fclb+fcl+fclt)/8).toFixed(1);
        var total3= parseFloat(pTotal);
        var t3 = total3 + response;
        var final = (t3/3).toFixed(1);
        console.log(final);
        $("#promedioTotalVerde").html(pTotal);
        $("#encuestas").html("Total encuestas: "+results.length);
        $("#pgeneral").html(final);
        $("#servicios").html('Automotriz')

        },
        error: function(error) {
            alert("Error: " + error.code + " " + error.message);
        }
        });

    })
}

IndresEmocional();
